package com.sample.android.base

class ConfigImpl : Config {

    override val crmUrl: String = "https://example.com/api/v1/"
    override val crmAppToken: String = "4941b968-5ec1-4c88-b9fc-a1760dc99296"

    override val hostessUrl: String = "https://example.com/api/v1/"
    override val hostessAppToken: String = "e6c863f7-983a-47fe-a09f-1f4c93373d4e"

    override val navigatorUrl: String = "https://example.com/api/v1/"
    override val navigatorAppToken: String = "3e0c81ea-ab14-4b83-b0b5-c9ccee925706"
}