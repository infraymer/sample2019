package com.sample.android.base

interface Config {
    val crmUrl: String
    val crmAppToken: String

    val hostessUrl: String
    val hostessAppToken: String

    val navigatorUrl: String
    val navigatorAppToken: String
}