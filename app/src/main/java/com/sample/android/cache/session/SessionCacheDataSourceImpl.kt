package com.sample.android.cache.session

import android.content.SharedPreferences
import androidx.core.content.edit
import io.reactivex.Single
import com.sample.android.data.session.source.SessionCacheDataSource
import com.sample.android.view.injection.qualifiers.AppSharedPreferences
import javax.inject.Inject

class SessionCacheDataSourceImpl
@Inject
constructor(
    @AppSharedPreferences
    private val sharedPreferences: SharedPreferences
) : SessionCacheDataSource {

    companion object {
        private const val IS_FIRST_LAUNCH_APP = "is_first_launch_app"
    }

    override fun isFirstSession(): Single<Boolean> =
        Single.fromCallable {
            val value = sharedPreferences.getBoolean(IS_FIRST_LAUNCH_APP, true)
            sharedPreferences.edit { putBoolean(IS_FIRST_LAUNCH_APP, false) }
            value
        }
}