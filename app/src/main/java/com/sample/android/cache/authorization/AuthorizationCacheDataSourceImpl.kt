package com.sample.android.cache.authorization

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Maybe
import com.sample.android.data.authorization.source.AuthorizationCacheDataSource
import com.sample.android.domain.authorization.model.AuthData
import com.sample.android.remote.base.AuthInterceptor
import com.sample.android.remote.base.NavigatorAuthInterceptor
import com.sample.android.view.injection.qualifiers.AppSharedPreferences
import javax.inject.Inject

class AuthorizationCacheDataSourceImpl
@Inject
constructor(
    @AppSharedPreferences
    private val sharedPreferences: SharedPreferences
) : AuthorizationCacheDataSource {

    companion object {
        private const val AUTH_USER = "auth_user"
    }

    override fun getAuthData(): Maybe<AuthData> =
        Maybe.create {
            val json = sharedPreferences.getString(AUTH_USER, null)
            if (json == null) {
                it.onComplete()
                return@create
            }
            val data = Gson().fromJson(json, AuthData::class.java)
            it.onSuccess(data)
        }

    override fun setAuthData(data: AuthData): Completable =
        Completable.fromAction {
            val json = Gson().toJson(data)
            sharedPreferences.edit {
                putString(AUTH_USER, json)
                putString(AuthInterceptor.TOKEN, data.token)
                putLong(NavigatorAuthInterceptor.USER_ID, data.userId)
            }
        }
}