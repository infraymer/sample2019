package com.sample.android.remote.user.mapper

import com.sample.android.base.ImageUtil
import com.sample.android.domain.user.model.User
import com.sample.android.remote.user.model.UserResponse

fun UserResponse.toUser(): User =
    User(
        id = userId,
        name = name,
        surname = surname,
        phone = phone,
        email = email,
        avatar = avatar.takeIf { it.isNotEmpty() }?.let(ImageUtil::convert)
    )
