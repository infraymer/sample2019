package com.sample.android.remote.restaurant_rating.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import com.sample.android.domain.restaurant_rating.model.RestaurantRating
import com.sample.android.remote.base.model.BaseResponse

interface RestaurantRatingService {
    @GET("crm/feedback/reviews/assessment")
    fun getRestaurantRating(@Query("place") primepassId: String): Single<BaseResponse<List<RestaurantRating>>>
}