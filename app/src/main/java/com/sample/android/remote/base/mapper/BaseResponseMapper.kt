package com.sample.android.remote.base.mapper

import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.HttpException
import com.sample.android.domain.base.exception.ServerInternalError
import com.sample.android.remote.base.model.BaseResponse

fun <T> Single<T>.handleResponse(): Single<T> =
    onErrorResumeNext { t ->
        if (t is HttpException && t.code() == 500) {
            Single.error(t.toServerInternalError())
        } else {
            Single.error(t)
        }
    }

fun <T> Single<BaseResponse<T>>.handleBaseResponse(): Single<T> =
    flatMap { it.handleError() }
        .map { it.data!! }
        .onErrorResumeNext { t ->
            if (t is HttpException && t.code() == 500) {
                Single.error(t.toServerInternalError())
            } else {
                Single.error(t)
            }
        }

fun Completable.handleBaseResponse(): Completable =
    onErrorResumeNext { t ->
        if (t is HttpException && t.code() == 500) {
            Completable.error(t.toServerInternalError())
        } else {
            Completable.error(t)
        }
    }

private fun <T> BaseResponse<T>.handleError(): Single<BaseResponse<T>> =
    when (status) {
        BaseResponse.Status.OK ->
            Single.just(this)
        else ->
            Single.error(RuntimeException("Server exception (status = $status)"))
    }

private fun HttpException.toServerInternalError(): ServerInternalError {
    val gson = Gson()
    val jsonString = response().errorBody()?.string()
    val response = gson.fromJson<BaseResponse<Unit>>(jsonString, BaseResponse::class.java)
    return ServerInternalError(response.error?.text ?: "")
}