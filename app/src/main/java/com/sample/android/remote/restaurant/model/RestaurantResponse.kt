package com.sample.android.remote.restaurant.model

import com.google.gson.annotations.SerializedName
import com.sample.android.domain.tag.model.Tag

class RestaurantResponse(
    @SerializedName("item")
    val item: RestaurantDataResponse,

    @SerializedName("tags")
    val tags: List<Tag>
)