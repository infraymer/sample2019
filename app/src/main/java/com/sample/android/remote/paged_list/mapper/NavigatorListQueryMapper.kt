package com.sample.android.remote.paged_list.mapper

import com.sample.android.domain.paged_list.model.NavigatorListQuery
import com.sample.android.remote.base.serialization.UTCDateAdapter
import javax.inject.Inject

class NavigatorListQueryMapper
@Inject
constructor() {
    companion object {
        private const val KEY_PAGE = "page"
        private const val KEY_PAGE_SIZE = "pageSize"

        private const val KEY_ID = "id"
        private const val KEY_TAG_SELECTOR = "query[dictionary_data.tags]"
        private const val KEY_EVENT_SELECTOR = "query[dictionary_data.events]"
        private const val KEY_RESTAURANT_SELECTOR = "query[dictionary_data.restaurants]"

        private const val KEY_TITLE_SELECTOR = "query[dictionary_data.title][\$regex]"
        private const val KEY_UPPER_DATE_SELECTOR = "query[dictionary_data.schedule.end][\$lte]"
        private const val KEY_LOWER_DATE_SELECTOR = "query[dictionary_data.schedule.start][\$gte]"

        private fun MutableMap<String, String>.putIfNotNull(key: String, value: String?) =
            value?.let { this[key] = it }
    }

    private val dateAdapter = UTCDateAdapter()

    fun mapToQueryMap(navigatorListQuery: NavigatorListQuery): Map<String, String> =
        hashMapOf<String, String>()
            .apply {
                this[KEY_PAGE] = navigatorListQuery.page.toString()
                this[KEY_PAGE_SIZE] = navigatorListQuery.pageSize.toString()

                putIfNotNull(KEY_ID, navigatorListQuery.id)
                putIfNotNull(KEY_TAG_SELECTOR, navigatorListQuery.tag)

                putIfNotNull(KEY_EVENT_SELECTOR, navigatorListQuery.eventId)
                putIfNotNull(KEY_RESTAURANT_SELECTOR, navigatorListQuery.restaurantId)

                putIfNotNull(KEY_TITLE_SELECTOR, navigatorListQuery.titleSelector)

                val date = navigatorListQuery.dateSelector
                    ?.let(dateAdapter::dateToString)
                putIfNotNull(KEY_UPPER_DATE_SELECTOR, date)
                putIfNotNull(KEY_LOWER_DATE_SELECTOR, date)
            }
}