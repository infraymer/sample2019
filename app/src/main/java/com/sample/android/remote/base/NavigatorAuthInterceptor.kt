package com.sample.android.remote.base

import android.content.SharedPreferences
import okhttp3.Interceptor
import okhttp3.Response
import com.sample.android.view.injection.qualifiers.AppSharedPreferences
import javax.inject.Inject

class NavigatorAuthInterceptor
@Inject
constructor(
    @AppSharedPreferences
    private val sharedPreferences: SharedPreferences
) : Interceptor {

    companion object {
        const val USER_ID = "user_id"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val userId = sharedPreferences.getLong(USER_ID, -1)

        val request =
            if (userId != -1L) {
                chain.request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer $userId")
                    .build()
            } else {
                chain.request()
            }

        return chain.proceed(request)
    }
}