package com.sample.android.remote.restaurant.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import com.sample.android.remote.restaurant.model.RestaurantResponse

interface RestaurantService {
    @GET("screens/restaurant")
    fun getRestaurant(@Query("id") id: String): Single<RestaurantResponse>
}