package com.sample.android.remote.user.service

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query
import com.sample.android.remote.base.model.BaseResponse
import com.sample.android.remote.user.model.UpdateUserRequest
import com.sample.android.remote.user.model.UserResponse

interface UserService {

    @GET("crm/client")
    fun getUser(@Query("user_id") userId: Long): Single<BaseResponse<UserResponse>>

    @PUT("crm/client")
    fun updateUser(@Body request: UpdateUserRequest): Completable

    @GET("crm/card_info")
    fun getUserCardInfo(): Single<BaseResponse<Unit>>
}