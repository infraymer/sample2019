package com.sample.android.remote.taxi.model

import com.google.gson.annotations.SerializedName
import com.sample.android.domain.taxi.model.Taxi

class TaxiResponse(
    @SerializedName("partners")
    val partners: List<Taxi>
)