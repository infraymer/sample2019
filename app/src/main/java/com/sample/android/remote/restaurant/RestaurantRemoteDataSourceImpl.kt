package com.sample.android.remote.restaurant

import io.reactivex.Single
import com.sample.android.data.restaurant.source.RestaurantRemoteDataSource
import com.sample.android.domain.restaurant.model.RestaurantInfo
import com.sample.android.remote.restaurant.mapper.RestaurantInfoMapper
import com.sample.android.remote.restaurant.service.RestaurantService
import javax.inject.Inject

class RestaurantRemoteDataSourceImpl
@Inject
constructor(
    private val restaurantService: RestaurantService,
    private val restaurantInfoMapper: RestaurantInfoMapper
) : RestaurantRemoteDataSource {
    override fun getRestaurantInfo(restaurantId: String): Single<RestaurantInfo> =
        restaurantService
            .getRestaurant(restaurantId)
            .map(restaurantInfoMapper::mapToRestaurantInfo)
}