package com.sample.android.remote.authorization.service

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.POST
import retrofit2.http.Body
import retrofit2.http.PUT
import com.sample.android.domain.authorization.model.AuthData
import com.sample.android.domain.authorization.model.Authorization
import com.sample.android.remote.base.model.BaseResponse

interface AuthorizationService {

    @GET("crm/authorization")
    fun authorizationGetSms(@Query("login") phone: String, @Query("authorization_type") authType: String): Completable

    @POST("crm/authorization")
    fun authorizationConfirm(@Body authorization: Authorization): Single<BaseResponse<AuthData>>

    @GET("crm/password")
    fun recoveryPassword(): Completable

    @PUT("crm/password")
    fun changePassword(): Completable

    @PUT("crm/lost_password")
    fun confirmRecoveryPassword(): Completable
}