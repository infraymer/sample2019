package com.sample.android.remote.authorization

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.data.authorization.source.AuthorizationRemoteDataSource
import com.sample.android.domain.authorization.model.AuthData
import com.sample.android.domain.authorization.model.Authorization
import com.sample.android.remote.authorization.service.AuthorizationService
import com.sample.android.remote.base.mapper.handleBaseResponse
import javax.inject.Inject

class AuthorizationRemoteDataSourceImpl
@Inject
constructor(
    private val authService: AuthorizationService
) : AuthorizationRemoteDataSource {

    override fun authorizationGetSms(authorization: Authorization): Completable =
        authService.authorizationGetSms(authorization.phone, authorization.type.name)
            .handleBaseResponse()

    override fun authorizationConfirm(authorization: Authorization): Single<AuthData> =
        authService.authorizationConfirm(authorization)
            .handleBaseResponse()

    override fun recoveryPassword(): Completable =
        authService.recoveryPassword()
            .handleBaseResponse()

    override fun changePassword(): Completable =
        authService.changePassword()
            .handleBaseResponse()

    override fun confirmRecoveryPassword(): Completable =
        authService.confirmRecoveryPassword()
            .handleBaseResponse()
}