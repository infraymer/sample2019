package com.sample.android.remote.user

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.data.user.source.UserRemoteDataSource
import com.sample.android.domain.user.model.User
import com.sample.android.remote.base.mapper.handleBaseResponse
import com.sample.android.remote.user.mapper.toUpdateUserRequest
import com.sample.android.remote.user.mapper.toUser
import com.sample.android.remote.user.service.UserService
import javax.inject.Inject

class UserRemoteDataSourceImpl
@Inject
constructor(
    private val userService: UserService
) : UserRemoteDataSource {

    override fun getUser(userId: Long): Single<User> =
        userService
            .getUser(userId)
            .handleBaseResponse()
            .map { it.toUser() }

    override fun updateUser(user: User): Completable =
        Single.just(user)
            .map { user.toUpdateUserRequest() }
            .flatMapCompletable { userService.updateUser(it) }
            .handleBaseResponse()

    override fun getUserCardInfo(): Single<Unit> =
        userService
            .getUserCardInfo()
            .handleBaseResponse()
}