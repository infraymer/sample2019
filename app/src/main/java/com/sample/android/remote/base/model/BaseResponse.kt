package com.sample.android.remote.base.model

import com.google.gson.annotations.SerializedName

class BaseResponse<T>(
    @JvmField
    @SerializedName("status")
    val status: Status,

    @JvmField
    @SerializedName("data")
    val data: T?,

    @JvmField
    @SerializedName("error")
    val error: ErrorResponse?
) {

    enum class Status {

        @SerializedName("OK")
        OK,

        @SerializedName("ERROR")
        ERROR
    }
}