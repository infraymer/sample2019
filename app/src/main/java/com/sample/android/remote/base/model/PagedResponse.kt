package com.sample.android.remote.base.model

interface PagedResponse {
    val page: Int
    val pageSize: Int
    val total: Int
}