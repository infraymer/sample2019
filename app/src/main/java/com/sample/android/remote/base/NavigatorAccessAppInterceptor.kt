package com.sample.android.remote.base

import okhttp3.Interceptor
import okhttp3.Response

class NavigatorAccessAppInterceptor
constructor(
    private val token: String
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .addHeader("x-app-token", token)
            .build()
        return chain.proceed(request)
    }
}