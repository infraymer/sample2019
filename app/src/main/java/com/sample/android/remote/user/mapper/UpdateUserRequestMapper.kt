package com.sample.android.remote.user.mapper

import com.sample.android.base.ImageUtil
import com.sample.android.domain.user.model.User
import com.sample.android.remote.user.model.UpdateUserRequest

fun User.toUpdateUserRequest(): UpdateUserRequest =
    UpdateUserRequest(
        userId = id,
        name = name ?: "",
        surname = surname ?: "",
        phone = phone ?: "",
        email = email ?: "",
        avatar = avatar?.let(ImageUtil::convert) ?: ""
    )