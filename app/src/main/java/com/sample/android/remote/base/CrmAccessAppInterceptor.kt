package com.sample.android.remote.base

import okhttp3.Interceptor
import okhttp3.Response

class CrmAccessAppInterceptor(
    private val token: String
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .addHeader("accept", "application/json")
            .addHeader("x-api-key", token)
            .build()
        return chain.proceed(request)
    }
}