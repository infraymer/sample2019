package com.sample.android.remote.restaurant.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import com.sample.android.domain.coordinates.model.Coordinates
import com.sample.android.domain.media.model.Media

class RestaurantDataResponse(
    @SerializedName("id")
    val id: String,

    @SerializedName("title")
    val title: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("address")
    val address: String?,

    @SerializedName("coordinates")
    val coordinates: Coordinates,

    @SerializedName("images")
    val images: List<Media>,

    @SerializedName("images360")
    val images360: List<Media>,

    @SerializedName("images360_100x100")
    val images360small: List<Media>,

    @SerializedName("tags_ids")
    val tagsIds: List<String>,

    @SerializedName("is_favorite")
    val isFavorite: Boolean,

    @SerializedName("phone")
    val phone: String?,

    @SerializedName("site")
    val site: String?,

    @SerializedName("weekly_schedule")
    val weeklySchedule: JsonObject,

    @SerializedName("primepass_id")
    val primepassId: String
)