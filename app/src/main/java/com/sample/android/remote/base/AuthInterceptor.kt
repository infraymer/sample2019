package com.sample.android.remote.base

import android.content.SharedPreferences
import okhttp3.Interceptor
import okhttp3.Response
import com.sample.android.view.injection.qualifiers.AppSharedPreferences
import javax.inject.Inject

class AuthInterceptor
@Inject
constructor(
    @AppSharedPreferences
    private val sharedPreferences: SharedPreferences
) : Interceptor {

    companion object {
        const val TOKEN = "token"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = sharedPreferences.getString(TOKEN, "") ?: ""

        val request =
            if (token.isNotEmpty()) {
                chain.request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer $token")
                    .build()
            } else {
                chain.request()
            }

        return chain.proceed(request)
    }
}