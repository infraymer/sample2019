package com.sample.android.remote.registration.service

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import com.sample.android.domain.registration.model.Registration
import com.sample.android.domain.user.model.User
import com.sample.android.remote.base.model.BaseResponse

interface RegistrationService {

    @POST("crm/client")
    fun createUser(@Body request: Registration): Single<BaseResponse<User>>

    @POST("crm/reg_confirm")
    fun confirmRegistration(): Completable
}