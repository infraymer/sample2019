package com.sample.android.remote.base.model

import com.google.gson.annotations.SerializedName

class ErrorResponse(
    @SerializedName("message")
    val text: String?,

    @JvmField
    @SerializedName("code")
    val code: Long?
)