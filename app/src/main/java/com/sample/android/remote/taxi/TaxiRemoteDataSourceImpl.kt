package com.sample.android.remote.taxi

import io.reactivex.Single
import com.sample.android.data.taxi.source.TaxiRemoteDataSource
import com.sample.android.domain.coordinates.model.Coordinates
import com.sample.android.domain.taxi.model.Taxi
import com.sample.android.remote.taxi.model.TaxiResponse
import com.sample.android.remote.taxi.service.TaxiService
import javax.inject.Inject

class TaxiRemoteDataSourceImpl
@Inject
constructor(
    private val taxiService: TaxiService
) : TaxiRemoteDataSource {
    override fun getTaxi(startCoordinates: Coordinates, endCoordinates: Coordinates): Single<List<Taxi>> =
        taxiService
            .getTaxi(
                startCoordinates.lng,
                startCoordinates.lat,
                endCoordinates.lng,
                endCoordinates.lat
            )
            .map(TaxiResponse::partners)
}