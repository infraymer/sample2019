package com.sample.android.remote.restaurant.mapper

import com.sample.android.domain.restaurant.model.RestaurantInfo
import com.sample.android.remote.restaurant.model.RestaurantResponse
import javax.inject.Inject

class RestaurantInfoMapper
@Inject
constructor() {
    fun mapToRestaurantInfo(restaurantResponse: RestaurantResponse): RestaurantInfo =
        RestaurantInfo(
            restaurantResponse.item.description,
            restaurantResponse.item.phone,
            restaurantResponse.item.site,
            restaurantResponse.item.images360,
            restaurantResponse.item.images360small
        )
}