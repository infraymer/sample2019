package com.sample.android.remote.registration

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.data.registration.source.RegistrationRemoteDataSource
import com.sample.android.domain.registration.model.Registration
import com.sample.android.domain.user.model.User
import com.sample.android.remote.base.mapper.handleBaseResponse
import com.sample.android.remote.registration.service.RegistrationService
import javax.inject.Inject

class RegistrationRemoteDataSourceImpl
@Inject
constructor(
    private val registrationService: RegistrationService
) : RegistrationRemoteDataSource {
    override fun createUser(registration: Registration): Single<User> =
        registrationService.createUser(registration)
            .handleBaseResponse()

    override fun confirmRegistration(): Completable =
        registrationService.confirmRegistration()
}