package com.sample.android.remote.user.model

import com.google.gson.annotations.SerializedName

class UserResponse(

    @SerializedName("name")
    val name: String,

    @SerializedName("surname")
    val surname: String,

    @SerializedName("phone")
    val phone: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("client_photo")
    val avatar: String,

    @SerializedName("user_id")
    val userId: Long = 0
)