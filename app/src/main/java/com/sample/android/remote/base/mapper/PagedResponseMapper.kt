package com.sample.android.remote.base.mapper

import com.sample.android.domain.base.model.PagedList
import com.sample.android.remote.base.model.PagedResponse

inline fun <E, R : PagedResponse> R.toPagedList(selector: (R) -> List<E>): PagedList<E> =
    PagedList(selector(this), page = page, hasNext = pageSize * page < total, hasPrev = page > 1, total = total)