package com.sample.android.remote.restaurant_rating

import io.reactivex.Maybe
import com.sample.android.data.restaurant_rating.source.RestaurantRatingRemoteDataSource
import com.sample.android.domain.base.util.maybeFirst
import com.sample.android.domain.restaurant_rating.model.RestaurantRating
import com.sample.android.remote.base.mapper.handleBaseResponse
import com.sample.android.remote.restaurant_rating.service.RestaurantRatingService
import javax.inject.Inject

class RestaurantRatingRemoteDataSourceImpl
@Inject
constructor(
    private val restaurantRatingService: RestaurantRatingService
) : RestaurantRatingRemoteDataSource {
    override fun getRestaurantRating(primepassId: String): Maybe<RestaurantRating> =
        restaurantRatingService
            .getRestaurantRating(primepassId)
            .handleBaseResponse()
            .maybeFirst()
}