package com.sample.android.remote.taxi.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import com.sample.android.remote.taxi.model.TaxiResponse

interface TaxiService {
    @GET("taxi")
    fun getTaxi(
        @Query("start_lng") startLng: Double,
        @Query("start_lat") startLat: Double,
        @Query("end_lng") endLng: Double,
        @Query("end_lat") endLat: Double
    ): Single<TaxiResponse>
}