package com.sample.android.view.restaurant.ui.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_restaurant.*
import kotlinx.android.synthetic.main.layout_related_events.*
import kotlinx.android.synthetic.main.layout_restaurant_checkout.*
import ru.nobird.android.ui.adapters.DefaultDelegateAdapter
import ru.nobird.android.ui.adapters.selection.SingleChoiceSelectionHelper
import ru.nobird.android.ui.viewstatedelegate.ViewStateDelegate
import com.sample.android.App
import com.sample.android.R
import com.sample.android.domain.comment.model.CommentItem
import com.sample.android.domain.coordinates.resolver.CoordinatesResolver
import com.sample.android.domain.event_list.model.EventListItem
import com.sample.android.domain.restaurant_map.model.RestaurantListItem
import com.sample.android.presentation.restaurant.RestaurantPresenter
import com.sample.android.presentation.restaurant.RestaurantView
import com.sample.android.view.base.ui.adapter.layoutmanager.WrappingLinearLayoutManager
import com.sample.android.view.base.ui.dialog.RoundedBottomSheetDialogFragment
import com.sample.android.view.base.ui.fragment.argument
import com.sample.android.view.calendar.ui.fragment.CalendarBottomSheetFragment
import com.sample.android.view.event_list.ui.adapter.delegate.RelatedEventAdapterDelegate
import com.sample.android.view.restaurant.model.CheckoutTimeItem
import com.sample.android.view.restaurant.ui.adapter.decorators.RightMarginItemDecoration
import com.sample.android.view.restaurant.ui.adapter.delegates.CheckoutTimeAdapterDelegate
import com.sample.android.view.restaurant.ui.adapter.delegates.RestaurantCommentAdapterDelegate
import com.sample.android.view.restaurant.ui.delegates.CalendarViewDelegate
import com.sample.android.view.restaurant.ui.delegates.GuestSetterViewDelegate
import com.sample.android.view.restaurant.ui.delegates.RestaurantHeaderViewDelegate
import com.sample.android.view.restaurant.ui.delegates.RestaurantLocationActionsDelegate
import com.sample.android.view.restaurant.ui.delegates.RestaurantAdditionalInfoDelegate
import com.sample.android.view.restaurant.ui.delegates.RestaurantRatingViewDelegate
import com.sample.android.view.util.extension.dip
import com.sample.android.view.util.extension.toast
import java.util.Date
import javax.inject.Inject

class RestaurantBottomSheetFragment : RoundedBottomSheetDialogFragment(), RestaurantView {
    companion object {
        const val TAG = "RestaurantBottomSheetFragment"

        fun newInstance(restaurantListItem: RestaurantListItem): DialogFragment =
            RestaurantBottomSheetFragment()
                .apply {
                    this.restaurantListItem = restaurantListItem
                }
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var presenter: RestaurantPresenter

    private var restaurantListItem: RestaurantListItem by argument()

    private lateinit var checkoutTimeAdapter: DefaultDelegateAdapter<CheckoutTimeItem>

    private lateinit var relatedEventsAdapter: DefaultDelegateAdapter<EventListItem>
    private lateinit var commentsAdapter: DefaultDelegateAdapter<CommentItem>

    private lateinit var calendarViewDelegate: CalendarViewDelegate
    private lateinit var guestSetterViewDelegate: GuestSetterViewDelegate
    private lateinit var restaurantHeaderViewDelegate: RestaurantHeaderViewDelegate

    private lateinit var restaurantLocationActionsDelegate: RestaurantLocationActionsDelegate
    private lateinit var restaurantAdditionalInfoDelegate: RestaurantAdditionalInfoDelegate
    private lateinit var restaurantRatingViewDelegate: RestaurantRatingViewDelegate

    private lateinit var viewStateDelegate: ViewStateDelegate<RestaurantView.State>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()

        presenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(RestaurantPresenter::class.java)

        presenter.onRestaurantListItem(restaurantListItem)

        checkoutTimeAdapter = DefaultDelegateAdapter()
        val selectionHelper = SingleChoiceSelectionHelper(checkoutTimeAdapter)
        checkoutTimeAdapter += CheckoutTimeAdapterDelegate(selectionHelper) {
            selectionHelper.select(checkoutTimeAdapter.items.indexOf(it))
        }

        relatedEventsAdapter = DefaultDelegateAdapter()
        relatedEventsAdapter += RelatedEventAdapterDelegate { /* todo: show event details */ }

        commentsAdapter = DefaultDelegateAdapter()
        commentsAdapter += RestaurantCommentAdapterDelegate {}
    }

    private fun injectComponent() {
        App.component()
            .restaurantComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_restaurant, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewStateDelegate = ViewStateDelegate()
        viewStateDelegate.addState<RestaurantView.State.Idle>()
        viewStateDelegate.addState<RestaurantView.State.Loading>(restaurantEmptyProgress)
        viewStateDelegate.addState<RestaurantView.State.NetworkError>(restaurantEmptyNetwork)
        viewStateDelegate.addState<RestaurantView.State.Remote>(
            restaurantInfo, restaurantActions, restaurantRelatedEvents,
            restaurantRating, restaurantComments, restaurantLocationActions, restaurantAdditionalInfo
        )

        with(timeRecyclerView) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = checkoutTimeAdapter
            addItemDecoration(RightMarginItemDecoration(dip(5)))
        }

        checkoutTimeAdapter.items = listOf(
            CheckoutTimeItem.TimeItem("12:00"),
            CheckoutTimeItem.TimeItem("13:00"),
            CheckoutTimeItem.TimeItem("14:00"),
            CheckoutTimeItem.TimeItem("15:00"),
            CheckoutTimeItem.TimeItem("16:00"),
            CheckoutTimeItem.TimeItem("17:00")
        )

        calendarViewDelegate = CalendarViewDelegate(calendarLayout).apply {
            onCalendarClicked = { showCalendarDialog(Date().time) }
        }
        guestSetterViewDelegate = GuestSetterViewDelegate(guestLayout)

        /**
         * Header
         */
        restaurantHeaderViewDelegate = RestaurantHeaderViewDelegate(headerLayout)
        restaurantHeaderViewDelegate.setTitle(restaurantListItem.restaurantItem.title)
        restaurantHeaderViewDelegate.setImage(restaurantListItem.restaurantItem.images.firstOrNull()?.image ?: "")
        restaurantHeaderViewDelegate.setDistance(
            restaurantListItem.coordinates
                ?.let { CoordinatesResolver.distanceInKmBetweenEarthCoordinates(it, restaurantListItem.restaurantItem.coordinates) }
        )
        restaurantHeaderViewDelegate.setAddress(restaurantListItem.restaurantItem.address)
        restaurantHeaderViewDelegate.setRating(restaurantListItem.rating)

        /**
         * Rating
         */
        restaurantRatingViewDelegate = RestaurantRatingViewDelegate(restaurantRating)
        restaurantRatingViewDelegate.setRestaurantRating(restaurantListItem.rating)

        restaurantInfo.setOnClickListener { restaurantInfo.toggle() }

        relatedEventsAll.isGone = true
//        relatedEventsAll.setOnClickListener { /* todo: show all events */ }

        with(relatedEventsRecycler) {
            adapter = relatedEventsAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }

        with(restaurantComments) {
            adapter = commentsAdapter
            layoutManager = WrappingLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false, 1)
            LinearSnapHelper().attachToRecyclerView(this)
        }

        restaurantLocationActionsDelegate = RestaurantLocationActionsDelegate(
            containerView = restaurantLocationActions,
            onAddressClicked = { presenter.onCoordinatesClicked(it.coordinates) },
            onTaxiClicked = { presenter.onLinkClicked(it.url) }
        )

        restaurantAdditionalInfoDelegate = RestaurantAdditionalInfoDelegate(
            containerView = restaurantAdditionalInfo,
            onLinkClicked = presenter::onLinkClicked,
            onPhoneClicked = presenter::onPhoneClicked
        )
//        restaurantAdditionalInfoDelegate.setSchedule("пн-вс", "11:00 – 00:00")
        restaurantAdditionalInfoDelegate.facebook = null
        restaurantAdditionalInfoDelegate.instagram = null
    }

    override fun onStart() {
        (view?.parent as? View)?.let { parent ->
            val behavior = BottomSheetBehavior.from(parent)
            behavior.peekHeight = resources.getDimensionPixelOffset(R.dimen.restaurant_peek_height)
        }
        super.onStart()
        presenter.attachView(this)
    }

    override fun onStop() {
        presenter.detachView(this)
        super.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CalendarBottomSheetFragment.REQUEST_CODE ->
                data?.takeIf { resultCode == Activity.RESULT_OK }
                    ?.getLongExtra(CalendarBottomSheetFragment.ARG_TIME, 0)
                    ?.let { context?.toast(it.toString()) }

            else ->
                super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun setState(state: RestaurantView.State) {
        viewStateDelegate.switchState(state)

        when (state) {
            is RestaurantView.State.Remote ->
                with(state.restaurant) {
                    restaurantInfo.text = info.description
                    restaurantLocationActionsDelegate.setAddress(address)

                    restaurantAdditionalInfoDelegate.link = info.site?.takeIf(String::isNotBlank)
                    restaurantAdditionalInfoDelegate.phone = info.phone?.takeIf(String::isNotBlank)

                    restaurantRelatedEvents.isVisible = events.isNotEmpty()
                    relatedEventsAdapter.items = events
                    commentsAdapter.items = comments
                }
        }
    }

    private fun showCalendarDialog(time: Long) {
        val supportFragmentManager = activity
            ?.supportFragmentManager
            ?.takeIf { it.findFragmentByTag(CalendarBottomSheetFragment.TAG) == null }
            ?: return

        val requestCode = CalendarBottomSheetFragment.REQUEST_CODE

        val dialog = CalendarBottomSheetFragment.newInstance(time)
        dialog.setTargetFragment(this, requestCode)
        dialog.show(supportFragmentManager, CalendarBottomSheetFragment.TAG)
    }
}