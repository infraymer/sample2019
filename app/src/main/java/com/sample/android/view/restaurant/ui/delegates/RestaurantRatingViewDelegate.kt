package com.sample.android.view.restaurant.ui.delegates

import android.view.View
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_restaurant_rating.*
import com.sample.android.R
import com.sample.android.domain.restaurant_rating.model.RestaurantRating

class RestaurantRatingViewDelegate(
    override val containerView: View
) : LayoutContainer {
    fun setRestaurantRating(restaurantRating: RestaurantRating) {
        restaurant_rating.rating = restaurantRating.rating

        restaurant_rating_count.text =
            containerView.resources.getQuantityString(R.plurals.reviews, restaurantRating.number, restaurantRating.number)

        restaurant_rating_text.text =
            containerView.context.getString(R.string.restaurant_rating, restaurantRating.rating)
    }
}