package com.sample.android.view.profile_edit.listener

interface ChangeAvatarListener {

    fun onReplace()

    fun onDelete()
}