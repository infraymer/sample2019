package com.sample.android.view.main.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_main.*
import com.sample.android.App
import com.sample.android.R
import com.sample.android.presentation.main.MainPresenter
import com.sample.android.presentation.main.MainView
import com.sample.android.view.base.navigation.RetainedAppNavigator
import com.sample.android.view.base.ui.fragment.FlowFragment
import com.sample.android.view.base.ui.listener.BackNavigable
import com.sample.android.view.main.navigation.screen.MainScreenTab
import javax.inject.Inject

class MainFragment : FlowFragment(), MainView {
    companion object {
        private const val ARG_TAB = "selectedTab"

        fun newInstance(tab: MainScreenTab = MainScreenTab.RESTAURANT_MAP): Fragment =
            MainFragment().apply {
                arguments = bundleOf(ARG_TAB to tab.ordinal)
            }
    }

    override val containerId: Int = R.id.main_fragment_container
    override val navigator by lazy {
        RetainedAppNavigator(
            childFragmentManager,
            containerId,
            ::onScreenChanged,
            ::onNeedNavigateBack
        )
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mainPresenter: MainPresenter

    private var selectedTab: MainScreenTab = MainScreenTab.RESTAURANT_MAP
        set(value) {
            setTab(field, false)
            setTab(value, true)
            field = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()

        val ordinal = savedInstanceState
            ?.getInt(ARG_TAB, -1)
            ?.takeIf { it != -1 }
            ?: arguments?.getInt(ARG_TAB, -1)
            ?: -1

        val screenTab = MainScreenTab.values()
            .getOrNull(ordinal)
            ?: MainScreenTab.RESTAURANT_MAP

        router.replaceScreen(screenTab.screen)

        mainPresenter = ViewModelProviders.of(this, viewModelFactory).get(MainPresenter::class.java)
    }

    private fun injectComponent() {
        App.component()
            .mainComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        main_tab_events
            .setOnClickListener { onTabClicked(MainScreenTab.EVENTS) }

        main_tab_restaurants
            .setOnClickListener { onTabClicked(MainScreenTab.RESTAURANT_MAP) }

        main_tab_profile
            .setOnClickListener { onTabClicked(MainScreenTab.PROFILE) }

        selectedTab = selectedTab
    }

    override fun onStart() {
        super.onStart()
        mainPresenter.attachView(this)
    }

    override fun onStop() {
        mainPresenter.detachView(this)
        super.onStop()
    }

    private fun onTabClicked(tab: MainScreenTab) {
        router.replaceScreen(tab.screen)
    }

    private fun setTab(mainScreenTab: MainScreenTab, isSelected: Boolean) {
        val tab =
            when (mainScreenTab) {
                MainScreenTab.EVENTS ->
                    main_tab_events
                MainScreenTab.RESTAURANT_MAP ->
                    main_tab_restaurants
                MainScreenTab.PROFILE ->
                    main_tab_profile
            }

        tab.isSelected = isSelected
    }

    private fun onScreenChanged(screenKey: String) {
        selectedTab = MainScreenTab
            .values()
            .find { it.screen.screenKey == screenKey }
            ?: throw IllegalStateException("Unexpected screen key = $screenKey")
    }

    private fun onNeedNavigateBack() {
        val backNavigable = (parentFragment as? BackNavigable)
            ?: (activity as? BackNavigable)
            ?: return

        backNavigable.onNavigateBack()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(ARG_TAB, selectedTab.ordinal)
    }
}