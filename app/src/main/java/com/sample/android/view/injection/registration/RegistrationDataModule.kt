package com.sample.android.view.injection.registration

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.registration.repository.RegistrationRepositoryImpl
import com.sample.android.data.registration.source.RegistrationRemoteDataSource
import com.sample.android.domain.registration.repository.RegistrationRepository
import com.sample.android.remote.registration.RegistrationRemoteDataSourceImpl
import com.sample.android.remote.registration.service.RegistrationService
import com.sample.android.view.injection.qualifiers.Crm

@Module
internal abstract class RegistrationDataModule {

    @Binds
    internal abstract fun bindRegistrationRepository(
        RegistrationRepositoryImpl: RegistrationRepositoryImpl
    ): RegistrationRepository

    @Binds
    internal abstract fun bindRegistrationRemoteDataSource(
        authRemoteDataSourceImpl: RegistrationRemoteDataSourceImpl
    ): RegistrationRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideRegistrationService(@Crm retrofit: Retrofit): RegistrationService =
            retrofit.create(RegistrationService::class.java)
    }
}