package com.sample.android.view.injection.app

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.sample.android.base.Config
import com.sample.android.base.ConfigImpl
import com.sample.android.presentation.base.injection.DaggerViewModelFactory
import com.sample.android.view.injection.qualifiers.AppSharedPreferences
import com.sample.android.view.injection.qualifiers.BackgroundScheduler
import com.sample.android.view.injection.qualifiers.MainScheduler

@Module
abstract class AppModule {
    @Binds
    internal abstract fun bindViewModelFactory(daggerViewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Module
    companion object {

        @Provides
        @JvmStatic
        internal fun provideConfig(): Config =
            ConfigImpl()

        @Provides
        @JvmStatic
        @MainScheduler
        internal fun provideAndroidScheduler(): Scheduler =
            AndroidSchedulers.mainThread()

        @Provides
        @JvmStatic
        @BackgroundScheduler
        internal fun provideBackgroundScheduler(): Scheduler =
            Schedulers.io()

        @Provides
        @JvmStatic
        @AppSharedPreferences
        internal fun provideSharedPreferences(context: Context): SharedPreferences =
            context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE)
    }
}