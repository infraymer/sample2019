package com.sample.android.view.injection.profile_feedback

import dagger.Subcomponent
import com.sample.android.view.profile_feedback.ui.fragment.ProfileFeedbackFragment

@Subcomponent(
    modules = [
        ProfileFeedbackModule::class
    ]
)
interface ProfileFeedbackComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): ProfileFeedbackComponent
    }

    fun inject(fragment: ProfileFeedbackFragment)
}