package com.sample.android.view.injection.comment

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.comment.repository.CommentRepositoryImpl
import com.sample.android.data.comment.source.CommentRemoteDataSource
import com.sample.android.domain.comment.repository.CommentRepository
import com.sample.android.remote.comment.CommentRemoteDataSourceImpl
import com.sample.android.remote.comment.service.CommentService
import com.sample.android.view.injection.qualifiers.Crm

@Module
internal abstract class CommentDataModule {
    @Binds
    internal abstract fun bindCommentRepository(
        repositoryImpl: CommentRepositoryImpl
    ): CommentRepository

    @Binds
    internal abstract fun bindCommentRemoteDataSource(
        remoteDataSourceImpl: CommentRemoteDataSourceImpl
    ): CommentRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideService(@Crm retrofit: Retrofit): CommentService =
            retrofit.create(CommentService::class.java)
    }
}