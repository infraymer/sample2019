package com.sample.android.view.main.navigation.screen

import ru.terrakok.cicerone.android.support.SupportAppScreen
import com.sample.android.view.event_list.navigation.screen.EventListScreen
import com.sample.android.view.profile.navigation.screen.MainProfileScreen
import com.sample.android.view.reustarants_map.navigation.screen.RestaurantsMapScreen

enum class MainScreenTab(val screen: SupportAppScreen) {
    EVENTS(EventListScreen),
    RESTAURANT_MAP(RestaurantsMapScreen),
    PROFILE(MainProfileScreen)
}