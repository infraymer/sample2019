package com.sample.android.view.auth.ui.delegates

import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_auth_sign_up.*
import com.sample.android.view.util.InputMask

class SignUpViewDelegate(
    override val containerView: View
) : LayoutContainer {

    var isIncorrectPhone: Boolean = false
        set(value) {
            field = value
            signUp_errorLabel.isVisible = value
        }

    var onGetCodeButtonClicked: (() -> Unit)? = null
    var onPhoneChanged: ((String, Boolean) -> Unit)? = null
    var onFirstNameChanged: ((String) -> Unit)? = null
    var onLastNameChanged: ((String) -> Unit)? = null

    init {
        MaskedTextChangedListener.installOn(
            signUp_phoneEditText,
            InputMask.PHONE,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(maskFilled: Boolean, extractedValue: String, formattedValue: String) {
                    onPhoneChanged?.invoke(extractedValue, maskFilled)
                }
            }
        )

        signUp_firstNameEditText.doOnTextChanged { text, _, _, _ -> onFirstNameChanged?.invoke(text.toString()) }
        signUp_lastNameEditText.doOnTextChanged { text, _, _, _ -> onLastNameChanged?.invoke(text.toString()) }

        signUp_getCodeButton.setOnClickListener { onGetCodeButtonClicked?.invoke() }

        isIncorrectPhone = false
    }
}