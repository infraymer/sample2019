package com.sample.android.view.injection.main

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.main.MainPresenter

@Module
abstract class MainModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainPresenter::class)
    internal abstract fun bindMainPresenter(mainPresenter: MainPresenter): ViewModel
}