package com.sample.android.view.restaurant.ui.delegates

import android.view.View
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_restaurant_checkout.*
import com.sample.android.R

class CalendarViewDelegate(
    override val containerView: View
) : LayoutContainer {

    enum class Day {
        TODAY, TOMORROW
    }

    var onCalendarClicked: () -> Unit = {}

    private var todayViewDelegate: DayCardViewDelegate
    private var tomorrowViewDelegate: DayCardViewDelegate

    private var selectedDay: Day = Day.TODAY
        set(value) {
            field = value
            changeStyle(value)
        }

    init {
        val resources = containerView.context.resources
        todayCardView.setOnClickListener { selectedDay = Day.TODAY }
        tomorrowCardView.setOnClickListener { selectedDay = Day.TOMORROW }
        calendarImageView.setOnClickListener { onCalendarClicked() }
        todayViewDelegate = DayCardViewDelegate(todayCardView).apply {
            name = resources.getString(R.string.restaurant_today)
            day = "21"
        }
        tomorrowViewDelegate = DayCardViewDelegate(tomorrowCardView).apply {
            name = resources.getString(R.string.restaurant_tomorrow)
            day = "22"
        }
        selectedDay = Day.TODAY
    }

    private fun changeStyle(day: Day) {
        todayViewDelegate.isSelected = day == Day.TODAY
        tomorrowViewDelegate.isSelected = day == Day.TOMORROW
    }
}