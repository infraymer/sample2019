package com.sample.android.view.injection.session

import dagger.Binds
import dagger.Module
import com.sample.android.cache.session.SessionCacheDataSourceImpl
import com.sample.android.data.session.repository.SessionRepositoryImpl
import com.sample.android.data.session.source.SessionCacheDataSource
import com.sample.android.domain.session.repository.SessionRepository

@Module
abstract class SessionModule {

    @Binds
    internal abstract fun bindSessionRepository(
        sessionRepositoryImpl: SessionRepositoryImpl
    ): SessionRepository

    @Binds
    internal abstract fun bindSessionCacheDataSource(
        sessionCacheDataSourceImpl: SessionCacheDataSourceImpl
    ): SessionCacheDataSource
}