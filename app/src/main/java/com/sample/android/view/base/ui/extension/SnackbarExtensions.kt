package com.sample.android.view.base.ui.extension

import android.widget.TextView
import androidx.annotation.ColorInt
import com.google.android.material.snackbar.Snackbar

fun Snackbar.setTextColor(@ColorInt textColor: Int): Snackbar =
    apply {
        view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
            .setTextColor(textColor)
    }