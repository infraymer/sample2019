package com.sample.android.view.injection.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class StethoInterceptor