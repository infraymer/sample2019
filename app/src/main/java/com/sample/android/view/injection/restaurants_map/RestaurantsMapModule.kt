package com.sample.android.view.injection.restaurants_map

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.restaurants_map.RestaurantsMapPresenter

@Module
internal abstract class RestaurantsMapModule {

    @Binds
    @IntoMap
    @ViewModelKey(RestaurantsMapPresenter::class)
    internal abstract fun bindRestaurantsMapPresenter(restaurantsMapPresenter: RestaurantsMapPresenter): ViewModel
}