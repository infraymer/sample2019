package com.sample.android.view.profile_edit.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_change_avatar.*
import com.sample.android.R
import com.sample.android.view.base.ui.dialog.RoundedBottomSheetDialogFragment
import com.sample.android.view.profile_edit.listener.ChangeAvatarListener

class ChangeAvatarBottomSheetDialogFragment : RoundedBottomSheetDialogFragment() {
    companion object {
        const val TAG = "ChangeAvatarBottomSheetDialogFragment"
        fun newInstance(): RoundedBottomSheetDialogFragment =
            ChangeAvatarBottomSheetDialogFragment()
    }

    private var changeAvatarListener: ChangeAvatarListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_change_avatar, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeAvatarListener = parentFragment as? ChangeAvatarListener

        replaceButton.setOnClickListener {
            changeAvatarListener?.onReplace()
            dismiss()
        }
        deleteButton.setOnClickListener {
            changeAvatarListener?.onDelete()
            dismiss()
        }
        cancelButton.setOnClickListener { dismiss() }
    }
}