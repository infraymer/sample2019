package com.sample.android.view.injection.event_list

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.event_list.EventListPresenter

@Module
internal abstract class EventListModule {
    @Binds
    @IntoMap
    @ViewModelKey(EventListPresenter::class)
    internal abstract fun bindEventsPresenter(eventsPresenter: EventListPresenter): ViewModel
}