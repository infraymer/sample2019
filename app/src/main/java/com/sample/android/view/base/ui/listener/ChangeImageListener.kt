package com.sample.android.view.base.ui.listener

interface ChangeImageListener {

    fun onReplace(position: Int)

    fun onDelete(position: Int)
}