package com.sample.android.view.calendar.ui.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.prolificinteractive.materialcalendarview.CalendarDay
import kotlinx.android.synthetic.main.fragment_calendar.*
import com.sample.android.R
import com.sample.android.view.base.ui.dialog.RoundedBottomSheetDialogFragment
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar

class CalendarBottomSheetFragment : RoundedBottomSheetDialogFragment() {

    companion object {
        const val TAG = "CalendarBottomSheetFragment"
        const val REQUEST_CODE = 2019
        const val ARG_TIME = "time"

        fun newInstance(time: Long): CalendarBottomSheetFragment =
            CalendarBottomSheetFragment().apply {
                arguments = bundleOf(
                    ARG_TIME to time
                )
            }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_calendar, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        calendarView.setOnDateChangedListener { _, date, _ ->
            val calendar = GregorianCalendar(date.year, date.month - 1, date.day)
            dateCallback(calendar.time.time)
            dismiss()
        }

        val time = arguments?.getLong(ARG_TIME, 0L) ?: Date().time

        calendarView.currentDate = toCalendarDate(time)
        calendarView.StateBuilder()
            .setMinimumDate(CalendarDay.today())
            .commit()
    }

    private fun toCalendarDate(time: Long): CalendarDay {
        val calendar = GregorianCalendar()
        calendar.time = Date(time)
        return CalendarDay.from(
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH) + 1,
            calendar.get(Calendar.DAY_OF_MONTH)
        )
    }

    private fun dateCallback(time: Long) {
        targetFragment
            ?.onActivityResult(
                targetRequestCode,
                Activity.RESULT_OK,
                Intent().putExtra(ARG_TIME, time)
            )
    }
}