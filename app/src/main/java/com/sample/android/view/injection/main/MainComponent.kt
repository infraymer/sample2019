package com.sample.android.view.injection.main

import dagger.Subcomponent
import com.sample.android.view.injection.session.SessionModule
import com.sample.android.view.main.ui.activity.MainActivity
import com.sample.android.view.main.ui.fragment.MainFragment

@Subcomponent(
    modules = [
        MainModule::class,
        SessionModule::class
    ]
)
interface MainComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): MainComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(mainFragment: MainFragment)
}