package com.sample.android.view.profile_pay_methods.navigation.screen

import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import com.sample.android.view.profile_pay_methods.ui.fragment.ProfilePayMethodsFragment

object ProfilePayMethodsScreen : SupportAppScreen() {

    override fun getFragment(): Fragment =
        ProfilePayMethodsFragment.newInstance()
}