package com.sample.android.view.injection.app

import com.sample.android.view.injection.search.SearchComponentHolder

class ComponentManager(
    appComponent: AppComponent
) {
    val searchCompoenentHolder = SearchComponentHolder(appComponent)
}