package com.sample.android.view.injection.restaurant

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.restaurant.repository.RestaurantRepositoryImpl
import com.sample.android.data.restaurant.source.RestaurantRemoteDataSource
import com.sample.android.domain.restaurant.repository.RestaurantRepository
import com.sample.android.remote.restaurant.RestaurantRemoteDataSourceImpl
import com.sample.android.remote.restaurant.service.RestaurantService
import com.sample.android.view.injection.qualifiers.Navigator

@Module
internal abstract class RestaurantDataModule {
    @Binds
    internal abstract fun bindRestaurantRepository(
        repositoryImpl: RestaurantRepositoryImpl
    ): RestaurantRepository

    @Binds
    internal abstract fun bindRestaurantRemoteDataSource(
        remoteDataSourceImpl: RestaurantRemoteDataSourceImpl
    ): RestaurantRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideRestaurantInfoService(@Navigator retrofit: Retrofit): RestaurantService =
            retrofit.create(RestaurantService::class.java)
    }
}