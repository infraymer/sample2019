package com.sample.android.view.restaurant.ui.adapter.delegates

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_comment.*
import ru.nobird.android.ui.adapterdelegates.AdapterDelegate
import ru.nobird.android.ui.adapterdelegates.DelegateViewHolder
import com.sample.android.R
import com.sample.android.domain.comment.model.CommentItem

class RestaurantCommentAdapterDelegate(
    private val onCommentClicked: (CommentItem) -> Unit
) : AdapterDelegate<CommentItem, DelegateViewHolder<CommentItem>>() {
    override fun isForViewType(position: Int, data: CommentItem): Boolean =
        true

    override fun onCreateViewHolder(parent: ViewGroup): DelegateViewHolder<CommentItem> =
        ViewHolder(createView(parent, R.layout.item_comment))

    inner class ViewHolder(
        override val containerView: View
    ) : DelegateViewHolder<CommentItem>(containerView), LayoutContainer {
        private val imageTransformation =
            MultiTransformation(CenterCrop(), RoundedCornersTransformation(context.resources.getDimensionPixelOffset(R.dimen.comment_image_radius), 0))

        init {
            containerView.setOnClickListener { (itemData as? CommentItem)?.let(onCommentClicked) }
        }

        override fun onBind(data: CommentItem) {
            Glide.with(commentImage)
                .load(data.user.avatar)
                .transform(imageTransformation)
                .into(commentImage)

            commentRating.rating = data.comment.assessment
            commentName.text = data.user.name
            commentText.text = data.comment.review
            commentTime.text = "3 days"
        }
    }
}