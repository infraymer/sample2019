package com.sample.android.view.injection.restaurant

import dagger.Subcomponent
import com.sample.android.view.injection.comment.CommentDataModule
import com.sample.android.view.injection.event_list.EventListDataModule
import com.sample.android.view.injection.taxi.TaxiDataModule
import com.sample.android.view.injection.user.UserDataModule
import com.sample.android.view.restaurant.ui.fragment.RestaurantBottomSheetFragment

@Subcomponent(modules = [
    CommentDataModule::class,
    EventListDataModule::class,
    RestaurantModule::class,
    RestaurantDataModule::class,
    TaxiDataModule::class,
    UserDataModule::class
])
interface RestaurantComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): RestaurantComponent
    }

    fun inject(fragment: RestaurantBottomSheetFragment)
}