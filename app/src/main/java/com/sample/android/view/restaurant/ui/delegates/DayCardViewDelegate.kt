package com.sample.android.view.restaurant.ui.delegates

import android.view.View
import com.google.android.material.card.MaterialCardView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_day.*
import com.sample.android.R
import com.sample.android.view.util.extension.dip
import com.sample.android.view.util.extension.getColorRes

class DayCardViewDelegate(
    override val containerView: View
) : LayoutContainer {

    var name: String
        get() = nameTextView.text.toString()
        set(value) {
            nameTextView.text = value
        }

    var dayOfWeek: String
        get() = dayOfWeekTextView.text.toString()
        set(value) {
            dayOfWeekTextView.text = value
        }

    var day: String
        get() = dayTextView.text.toString()
        set(value) {
            dayTextView.text = value
        }

    var isSelected: Boolean = false
        set(value) {
            field = value
            setSelectedStyle(value)
        }

    private fun setSelectedStyle(select: Boolean) {
        (containerView as MaterialCardView).apply {
            if (select) {
                cardElevation = context.dip(0f)
                strokeWidth = 0
                setCardBackgroundColor(context.getColorRes(R.color.black))
                dayTextView.setTextColor(context.getColorRes(R.color.white))
            } else {
                cardElevation = resources.getDimension(R.dimen.button_elevation)
                strokeColor = context.getColorRes(R.color.black_10)
                strokeWidth = resources.getDimension(R.dimen.card_border_width).toInt()
                setCardBackgroundColor(context.getColorRes(R.color.white))
                dayTextView.setTextColor(context.getColorRes(R.color.black))
            }
        }
    }
}