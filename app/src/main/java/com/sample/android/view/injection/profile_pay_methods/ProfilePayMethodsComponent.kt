package com.sample.android.view.injection.profile_pay_methods

import dagger.Subcomponent
import com.sample.android.view.profile_pay_methods.ui.fragment.ProfilePayMethodsFragment

@Subcomponent(
    modules = [
        ProfilePayMethodsModule::class
    ]
)
interface ProfilePayMethodsComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): ProfilePayMethodsComponent
    }

    fun inject(fragment: ProfilePayMethodsFragment)
}