package com.sample.android.view.injection.profile_edit

import dagger.Subcomponent
import com.sample.android.view.injection.auth.AuthDataModule
import com.sample.android.view.injection.registration.RegistrationDataModule
import com.sample.android.view.injection.user.UserDataModule
import com.sample.android.view.profile_edit.ui.fragment.ProfileEditFragment

@Subcomponent(
    modules = [
        ProfileEditModule::class,
        UserDataModule::class,
        AuthDataModule::class,
        RegistrationDataModule::class
    ]
)
interface ProfileEditComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): ProfileEditComponent
    }

    fun inject(profileEditFragment: ProfileEditFragment)
}