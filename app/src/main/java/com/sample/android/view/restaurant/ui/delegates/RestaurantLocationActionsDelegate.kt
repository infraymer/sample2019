package com.sample.android.view.restaurant.ui.delegates

import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.view.isGone
import androidx.core.view.isVisible
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_restaurant_location_actions.*
import com.sample.android.R
import com.sample.android.domain.restaurant.model.RestaurantAddress
import com.sample.android.domain.taxi.model.Taxi

class RestaurantLocationActionsDelegate(
    override val containerView: View,
    private val onAddressClicked: (RestaurantAddress) -> Unit,
    private val onTaxiClicked: (Taxi) -> Unit
) : LayoutContainer {
    init {
        restaurantLocation.setCompoundDrawablesWithIntrinsicBounds(null, null, restaurantLocation.icon, null)
        containerView.isGone = true
    }

    fun setAddress(address: RestaurantAddress?) {
        containerView.isVisible = address != null

        restaurantLocation.isVisible = address != null

        if (address != null) {
            restaurantLocation.text = address.address
            restaurantLocation.setOnClickListener { onAddressClicked(address) }
            restaurantLocation.isVisible = true
        } else {
            restaurantLocation.isVisible = false
        }

        val taxi = address?.taxi

        val taxiProvider =
            when (taxi?.partner) {
                Taxi.YANDEX -> TaxiProvider.YANDEX
                Taxi.UBER -> TaxiProvider.UBER
                else -> null
            }

        if (taxi != null && taxiProvider != null) {
            restaurantTaxi.setIconResource(taxiProvider.icon)

            val taxiTitle = containerView.context.getString(taxiProvider.title)
            restaurantTaxi.text = containerView.context.getString(R.string.taxi_pattern, taxiTitle, taxi.price)

            restaurantTaxi.setOnClickListener { onTaxiClicked(taxi) }
            restaurantTaxi.isVisible = true
        } else {
            restaurantTaxi.isVisible = false
        }
    }

    private enum class TaxiProvider(
        @DrawableRes
        val icon: Int,
        @StringRes
        val title: Int
    ) {
        YANDEX(R.drawable.ic_taxi_yandex, R.string.taxi_yandex),
        UBER(R.drawable.ic_taxi_yandex, R.string.taxi_uber)
    }
}