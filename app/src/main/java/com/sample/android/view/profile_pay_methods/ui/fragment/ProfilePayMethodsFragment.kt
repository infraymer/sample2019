package com.sample.android.view.profile_pay_methods.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_profile_pay_methods.*
import kotlinx.android.synthetic.main.layout_simple_appbar.*
import ru.nobird.android.ui.adapters.DefaultDelegateAdapter
import com.sample.android.App
import com.sample.android.R
import com.sample.android.presentation.profile_pay_methods.ProfilePayMethodsPresenter
import com.sample.android.presentation.profile_pay_methods.ProfilePayMethodsView
import com.sample.android.view.profile_pay_methods.model.PayMethodItem
import com.sample.android.view.profile_pay_methods.ui.adapter.delegates.AddPayMethodAdapterDelegate
import com.sample.android.view.profile_pay_methods.ui.adapter.delegates.PayMethodAdapterDelegate
import javax.inject.Inject


class ProfilePayMethodsFragment : Fragment(), ProfilePayMethodsView {
    companion object {
        fun newInstance(): Fragment =
            ProfilePayMethodsFragment()
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var presenter: ProfilePayMethodsPresenter

    private val payMethodsAdapter: DefaultDelegateAdapter<PayMethodItem> =
        DefaultDelegateAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        payMethodsAdapter += PayMethodAdapterDelegate()
        payMethodsAdapter += AddPayMethodAdapterDelegate()

        injectComponent()

        presenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(ProfilePayMethodsPresenter::class.java)
    }

    private fun injectComponent() {
        App.component()
            .profilePayMethodsComponent()
            .build()
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_profile_pay_methods, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleTextView.setText(R.string.profile_pay_methods_title)

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = payMethodsAdapter
        }

        payMethodsAdapter.items = listOf(
            PayMethodItem.PayMethod(),
            PayMethodItem.PayMethod(),
            PayMethodItem.PayMethod(),
            PayMethodItem.Add
        )
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onStop() {
        presenter.detachView(this)
        super.onStop()
    }
}
