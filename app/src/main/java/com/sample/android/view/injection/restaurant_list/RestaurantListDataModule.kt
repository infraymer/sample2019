package com.sample.android.view.injection.restaurant_list

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.restaurant_list.repository.RestaurantListRepositoryImpl
import com.sample.android.data.restaurant_list.source.RestaurantListRemoteDataSource
import com.sample.android.domain.restaurant_list.repository.RestaurantListRepository
import com.sample.android.remote.restaurant_list.RestaurantListRemoteDataSourceImpl
import com.sample.android.remote.restaurant_list.service.RestaurantListService
import com.sample.android.view.injection.qualifiers.Navigator

@Module
internal abstract class RestaurantListDataModule {
    @Binds
    internal abstract fun bindRestaurantListRepository(
        repositoryImpl: RestaurantListRepositoryImpl
    ): RestaurantListRepository

    @Binds
    internal abstract fun bindRestaurantListRemoteDataSource(
        remoteDataSourceImpl: RestaurantListRemoteDataSourceImpl
    ): RestaurantListRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideRestaurantListService(@Navigator retrofit: Retrofit): RestaurantListService =
            retrofit.create(RestaurantListService::class.java)
    }
}