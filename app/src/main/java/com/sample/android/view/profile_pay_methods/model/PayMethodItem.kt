package com.sample.android.view.profile_pay_methods.model

sealed class PayMethodItem {

    class PayMethod() : PayMethodItem()

    object Add : PayMethodItem()
}