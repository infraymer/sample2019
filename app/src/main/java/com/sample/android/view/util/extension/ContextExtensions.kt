package com.sample.android.view.util.extension

import android.app.ActivityManager
import android.content.Context
import android.widget.Toast
import androidx.core.content.ContextCompat

val Context.isMainProcess: Boolean
    get() {
        val pid = android.os.Process.myPid()
        return (getSystemService(Context.ACTIVITY_SERVICE) as? ActivityManager)
            ?.runningAppProcesses
            ?.find { it.pid == pid }
            ?.processName
            ?.let { it.isNotEmpty() && it == packageName }
            ?: false
    }

fun Context.getColorRes(resId: Int): Int =
    ContextCompat.getColor(this, resId)

fun Context.toast(text: String, long: Boolean = false) {
    Toast.makeText(this, text, if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT).show()
}