package com.sample.android.view.injection.event_list

import dagger.Subcomponent
import com.sample.android.view.event_list.ui.fragment.EventListFragment
import com.sample.android.view.injection.tag.TagsDataModule

@Subcomponent(modules = [
    EventListModule::class,
    EventListDataModule::class,
    TagsDataModule::class
])
interface EventListComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): EventListComponent
    }

    fun inject(eventListFragment: EventListFragment)
}