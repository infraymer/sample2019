package com.sample.android.view.injection.user

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.user.repository.UserRepositoryImpl
import com.sample.android.data.user.source.UserRemoteDataSource
import com.sample.android.domain.user.repository.UserRepository
import com.sample.android.remote.user.UserRemoteDataSourceImpl
import com.sample.android.remote.user.service.UserService
import com.sample.android.view.injection.qualifiers.Crm

@Module
internal abstract class UserDataModule {

    @Binds
    internal abstract fun bindUserRepository(
        UserRepositoryImpl: UserRepositoryImpl
    ): UserRepository

    @Binds
    internal abstract fun bindUserRemoteDataSource(
        authRemoteDataSourceImpl: UserRemoteDataSourceImpl
    ): UserRemoteDataSource

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideUserService(@Crm retrofit: Retrofit): UserService =
            retrofit.create(UserService::class.java)
    }
}