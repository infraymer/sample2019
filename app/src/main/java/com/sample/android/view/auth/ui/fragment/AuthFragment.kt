package com.sample.android.view.auth.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_auth.*
import kotlinx.android.synthetic.main.layout_auth.*
import ru.nobird.android.ui.viewstatedelegate.ViewStateDelegate
import com.sample.android.App
import com.sample.android.R
import com.sample.android.presentation.auth.AuthPresenter
import com.sample.android.presentation.auth.AuthView
import com.sample.android.view.auth.listener.AuthListener
import com.sample.android.view.auth.ui.delegates.CodeViewDelegate
import com.sample.android.view.auth.ui.delegates.SignInViewDelegate
import com.sample.android.view.auth.ui.delegates.SignUpViewDelegate
import com.sample.android.view.base.ui.listener.BackButtonListener
import com.sample.android.view.base.ui.listener.BackNavigable
import com.sample.android.view.base.ui.extension.setTextColor
import javax.inject.Inject

class AuthFragment : Fragment(), AuthView, BackButtonListener {
    companion object {
        private const val ARG_IS_CLOSABLE = "is_closable"

        fun newInstance(isClosable: Boolean = true): Fragment =
            AuthFragment().apply {
                arguments = bundleOf(ARG_IS_CLOSABLE to isClosable)
            }
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var presenter: AuthPresenter

    private lateinit var signInViewDelegate: SignInViewDelegate
    private lateinit var signUpViewDelegate: SignUpViewDelegate
    private lateinit var codeViewDelegate: CodeViewDelegate

    private val viewStateDelegate =
        ViewStateDelegate<AuthView.State>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()

        presenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(AuthPresenter::class.java)
    }

    private fun injectComponent() {
        App.component()
            .authComponent()
            .build()
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_auth, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signInButton.setOnClickListener { presenter.onSignInTabClicked() }
        signUpButton.setOnClickListener { presenter.onSignUpTabClicked() }
        closeButton.setOnClickListener { activity?.onBackPressed() }

        val isClosable = arguments?.getBoolean(ARG_IS_CLOSABLE) ?: true
        closeButton.isVisible = isClosable

        signInViewDelegate = SignInViewDelegate(signInLayout)
        signUpViewDelegate = SignUpViewDelegate(signUpLayout)
        codeViewDelegate = CodeViewDelegate(codeLayout)

        signInViewDelegate.apply {
            onGetCodeButtonClicked = { presenter.onGetCodeClicked() }
            onPhoneChanged = { phone, phoneFilled ->
                presenter.phoneAuth = phone
                presenter.phoneAuthFilled = phoneFilled
            }
        }

        signUpViewDelegate.apply {
            onGetCodeButtonClicked = { presenter.onSignUpClicked() }
            onFirstNameChanged = { presenter.firstName = it }
            onLastNameChanged = { presenter.lastName = it }
            onPhoneChanged = { phone, phoneFilled ->
                presenter.phoneReg = phone
                presenter.phoneRegFilled = phoneFilled
            }
        }

        codeViewDelegate.apply {
            onLoginButtonClicked = { presenter.onSignInClicked() }
            onCodeChanged = { presenter.code = it }
            onResetSmsButtonClicked = { presenter.onResendSmsClicked() }
        }

        initViewStateDelegate()
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true
    }

    override fun onDestroy() {
        codeViewDelegate.dispose()
        super.onDestroy()
    }

    private fun initViewStateDelegate() {
        viewStateDelegate.addState<AuthView.State.SignIn>(signInLayout, signInButton, signUpButton)
        viewStateDelegate.addState<AuthView.State.SignUp>(signUpLayout, signInButton, signUpButton)
        viewStateDelegate.addState<AuthView.State.Code>(codeLayout, smsTitleTextView)
    }

    private fun switchTab(state: AuthView.State) {
        signInButton.isSelected = state is AuthView.State.SignIn
        signUpButton.isSelected = state is AuthView.State.SignUp
    }

    override fun setState(state: AuthView.State) {
        when (state) {
            AuthView.State.Code -> {
                codeViewDelegate.resetTimer()
            }
        }
        switchTab(state)
        viewStateDelegate.switchState(state)
    }

    override fun showMessage(text: String) {
        val view = view
            ?: return

        Snackbar
            .make(view, text, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(view.context, R.color.white))
            .show()
    }

    override fun close(isAuthorized: Boolean) {
        (parentFragment as? AuthListener)
            ?.onCloseScreen(isAuthorized)
            ?: (parentFragment as? BackNavigable)
                ?.onNavigateBack()
            ?: (activity as? BackNavigable)
                ?.onNavigateBack()
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onStop() {
        presenter.detachView(this)
        super.onStop()
    }
}