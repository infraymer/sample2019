package com.sample.android.view.injection.event_list

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.event_list.repository.EventListRepositoryImpl
import com.sample.android.data.event_list.source.EventListRemoteDataSource
import com.sample.android.domain.event_list.repository.EventListRepository
import com.sample.android.remote.event_list.EventListRemoteDataSourceImpl
import com.sample.android.remote.event_list.service.EventListService
import com.sample.android.view.injection.qualifiers.Navigator

@Module
internal abstract class EventListDataModule {

    @Binds
    internal abstract fun bindEventListRepository(
        repositoryImpl: EventListRepositoryImpl
    ): EventListRepository

    @Binds
    internal abstract fun bindEventListRemoteDataSource(
        remoteDataSourceImpl: EventListRemoteDataSourceImpl
    ): EventListRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideEventListService(@Navigator retrofit: Retrofit): EventListService =
            retrofit.create(EventListService::class.java)
    }
}