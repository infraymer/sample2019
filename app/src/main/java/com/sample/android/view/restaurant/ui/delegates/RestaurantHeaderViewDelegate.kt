package com.sample.android.view.restaurant.ui.delegates

import android.graphics.Bitmap
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_restaurant_header.*
import com.sample.android.R
import com.sample.android.domain.restaurant_rating.model.RestaurantRating

class RestaurantHeaderViewDelegate(
    override val containerView: View,
    mode: Mode = Mode.HEADER
) : LayoutContainer {
    private val context = containerView.context
    private val imageTransformation: Transformation<Bitmap>

    init {
        val cornerType = if (mode != Mode.HEADER) RoundedCornersTransformation.CornerType.ALL else RoundedCornersTransformation.CornerType.TOP
        val borderRadius = if (mode != Mode.HEADER) R.dimen.restaurant_item_radius else R.dimen.restaurant_header_radius

        imageTransformation =
            MultiTransformation(
                CenterCrop(), RoundedCornersTransformation(containerView.resources.getDimensionPixelOffset(borderRadius), 0, cornerType)
            )
        handlerView.isVisible = mode == Mode.HEADER
        heartImageView.isGone = mode == Mode.SMALL_ITEM
        gradientImageView.setBackgroundResource(if (mode != Mode.HEADER) R.drawable.bg_rounded_overlay else R.drawable.gradient_restaurant)
    }

    fun setTitle(title: String) {
        restaurant_title.text = title
    }

    fun setImage(image: String) {
        Glide.with(restaurant_image)
            .load(image)
            .transform(imageTransformation)
            .into(restaurant_image)
    }

    fun setAddress(address: String) {
        restaurant_address.text = address
    }

    fun setRating(restaurantRating: RestaurantRating) {
        restaurant_rating.rating = restaurantRating.rating
        restaurant_rating_count.text = context.resources.getQuantityString(R.plurals.reviews, restaurantRating.number, restaurantRating.number)
    }

    fun setDistance(distance: Double?) {
        restaurant_distance_icon.isGone = distance == null
        restaurant_distance.isGone = distance == null

        if (distance != null) {
            restaurant_distance.text = context.getString(R.string.restaurant_distance, distance)
        }
    }

    enum class Mode {
        ITEM, SMALL_ITEM, HEADER
    }
}