package com.sample.android.view.injection.event

import dagger.Subcomponent
import com.sample.android.view.event.ui.fragment.EventFragment
import com.sample.android.view.injection.restaurant_list.RestaurantListDataModule
import com.sample.android.view.injection.restaurant_rating.RestaurantRatingDataModule

@Subcomponent(modules = [
    EventModule::class,
    EventDataModule::class,
    RestaurantListDataModule::class,
    RestaurantRatingDataModule::class
])
interface EventComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): EventComponent
    }

    fun inject(fragment: EventFragment)
}