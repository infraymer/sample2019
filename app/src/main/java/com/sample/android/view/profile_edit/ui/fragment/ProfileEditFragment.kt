package com.sample.android.view.profile_edit.ui.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.google.android.material.snackbar.Snackbar
import com.redmadrobot.inputmask.MaskedTextChangedListener
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import kotlinx.android.synthetic.main.fragment_profile_edit.*
import com.sample.android.App
import com.sample.android.R
import com.sample.android.domain.user.model.User
import com.sample.android.presentation.profile_edit.ProfileEditPresenter
import com.sample.android.presentation.profile_edit.ProfileEditView
import com.sample.android.view.base.ui.extension.setTextColor
import com.sample.android.view.base.ui.fragment.LoadingProgressDialogFragment
import com.sample.android.view.profile_edit.listener.ChangeAvatarListener
import com.sample.android.view.util.InputMask
import com.sample.android.view.util.ProgressHelper
import com.sample.android.view.util.extension.dip
import com.sample.android.view.util.extension.hideKeyboard
import javax.inject.Inject

class ProfileEditFragment : Fragment(), ProfileEditView, ChangeAvatarListener {

    companion object {
        const val IMAGE_PICKER_REQUEST_CODE = 101
        fun newInstance(): Fragment =
            ProfileEditFragment()
    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var presenter: ProfileEditPresenter

    private val progressDialogFragment: DialogFragment =
        LoadingProgressDialogFragment.newInstance()

    private var phone = ""
    private var avatar: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()

        presenter = ViewModelProviders
            .of(this, viewModelFactory)
            .get(ProfileEditPresenter::class.java)
    }

    private fun injectComponent() {
        App.component()
            .profileEditComponent()
            .build()
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_profile_edit, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val radius = dip(resources.getDimension(R.dimen.circle_radius)).toInt()
        val transformation =
            RoundedCornersTransformation(radius, 0)
        Glide.with(this)
            .load(R.drawable.ic_change_avatar)
            .transform(MultiTransformation(CenterCrop(), transformation))
            .into(changeImageView)

        changeImageView.setOnClickListener { showChangeAvatarDialog() }
        backButton.setOnClickListener { activity?.onBackPressed() }
        doneButton.setOnClickListener { presenter.onDoneClicked(getUserData()) }

        MaskedTextChangedListener.installOn(
            phoneEditText,
            InputMask.PHONE,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(maskFilled: Boolean, extractedValue: String, formattedValue: String) {
                    phone = extractedValue
                }
            }
        )

        showAvatar(null)
    }

    private fun getUserData(): User =
        User(
            name = nameEditText.text.toString(),
            surname = surnameEditText.text.toString(),
            phone = phone,
            email = emailEditText.text.toString(),
            avatar = avatar
        )

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onStop() {
        activity?.hideKeyboard()
        presenter.detachView(this)
        presenter.saveUserData(getUserData())
        super.onStop()
    }

    private fun showChangeAvatarDialog() {
        val supportFragmentManager = childFragmentManager
            .takeIf { it.findFragmentByTag(ChangeAvatarBottomSheetDialogFragment.TAG) == null }
            ?: return

        ChangeAvatarBottomSheetDialogFragment
            .newInstance()
            .show(supportFragmentManager, ChangeAvatarBottomSheetDialogFragment.TAG)
    }

    private fun showAvatar(bitmap: Bitmap?) {
        val radius = dip(resources.getDimension(R.dimen.circle_radius)).toInt()
        val transformation =
            RoundedCornersTransformation(radius, 0)
        Glide.with(this)
            .run { bitmap?.let(::load) ?: load(R.drawable.ic_profile) }
            .transform(MultiTransformation(CenterCrop(), transformation))
            .into(avatarImageView)
    }

    override fun onReplace() {
        presenter.onReplaceAvatarClicked()
    }

    override fun onDelete() {
        presenter.onDeleteAvatarClicked()
    }

    override fun showImagePicker() {
        val intent = Intent()
            .setAction(Intent.ACTION_GET_CONTENT)
            .setType("image/*")

        startActivityForResult(
            Intent.createChooser(intent, "Image Picker"),
            IMAGE_PICKER_REQUEST_CODE
        )
    }

    override fun setState(state: ProfileEditView.State) {
        when (state) {
            is ProfileEditView.State.Idle -> {
                ProgressHelper.dismiss(fragmentManager, LoadingProgressDialogFragment.TAG)
            }
            is ProfileEditView.State.Loaded -> {
                val data = state.data
                nameEditText.setText(data.name)
                surnameEditText.setText(data.surname)
                phoneEditText.setText(data.phone)
                emailEditText.setText(data.email)
                avatar = data.avatar
                showAvatar(data.avatar)
                ProgressHelper.dismiss(fragmentManager, LoadingProgressDialogFragment.TAG)
            }
            is ProfileEditView.State.Loading -> {
                activity?.hideKeyboard()
                ProgressHelper.activate(progressDialogFragment, fragmentManager, LoadingProgressDialogFragment.TAG)
            }
            is ProfileEditView.State.Complete -> {
                ProgressHelper.dismiss(fragmentManager, LoadingProgressDialogFragment.TAG)
            }
        }
    }

    override fun showMessage(text: String) {
        val view = view
            ?: return

        Snackbar
            .make(view, text, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(view.context, R.color.white))
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            IMAGE_PICKER_REQUEST_CODE ->
                data?.takeIf { resultCode == Activity.RESULT_OK }
                    ?.data
                    ?.let { uri ->
                        val bitmap = MediaStore.Images.Media.getBitmap(context?.contentResolver, uri)
                        presenter.onImageBitmap(bitmap)
                    }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}