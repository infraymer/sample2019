package com.sample.android.view.injection.profile

import dagger.Subcomponent
import com.sample.android.view.injection.auth.AuthDataModule
import com.sample.android.view.injection.registration.RegistrationDataModule
import com.sample.android.view.profile.ui.fragment.MainProfileFragment
import com.sample.android.view.profile.ui.fragment.ProfileFragment

@Subcomponent(modules = [
    ProfileModule::class,
    AuthDataModule::class,
    RegistrationDataModule::class
])
interface ProfileComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): ProfileComponent
    }

    fun inject(profileFragment: ProfileFragment)
    fun inject(mainProfileFragment: MainProfileFragment)
}