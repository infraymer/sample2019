package com.sample.android.view.restaurant_list.ui.adapter.delegate

import android.view.View
import android.view.ViewGroup
import ru.nobird.android.ui.adapterdelegates.AdapterDelegate
import ru.nobird.android.ui.adapterdelegates.DelegateViewHolder
import com.sample.android.R
import com.sample.android.domain.coordinates.resolver.CoordinatesResolver
import com.sample.android.domain.restaurant_map.model.RestaurantListItem
import com.sample.android.view.restaurant.ui.delegates.RestaurantHeaderViewDelegate

class RestaurantListItemAdapterDelegate(
    private val onItemClicked: (RestaurantListItem) -> Unit,
    private val isSmallItem: Boolean = false
) : AdapterDelegate<RestaurantListItem, DelegateViewHolder<RestaurantListItem>>() {
    override fun isForViewType(position: Int, data: RestaurantListItem): Boolean =
        true

    override fun onCreateViewHolder(parent: ViewGroup): DelegateViewHolder<RestaurantListItem> =
        ViewHolder(createView(parent, if (isSmallItem) R.layout.item_restaurant_small else R.layout.item_restaurant))

    private inner class ViewHolder(containerView: View) : DelegateViewHolder<RestaurantListItem>(containerView) {
        private val restaurantDelegate =
            RestaurantHeaderViewDelegate(
                containerView,
                mode = if (isSmallItem) RestaurantHeaderViewDelegate.Mode.SMALL_ITEM else RestaurantHeaderViewDelegate.Mode.ITEM)

        init {
            containerView.setOnClickListener { itemData?.let(onItemClicked) }
        }

        override fun onBind(data: RestaurantListItem) {
            restaurantDelegate.setTitle(data.title)
            restaurantDelegate.setAddress(data.restaurantItem.address)
            restaurantDelegate.setImage(data.restaurantItem.images.firstOrNull()?.image ?: "")
            restaurantDelegate.setRating(data.rating)
            restaurantDelegate.setDistance(
                data.coordinates
                    ?.let { CoordinatesResolver.distanceInKmBetweenEarthCoordinates(it, data.restaurantItem.coordinates) }
            )
        }
    }
}