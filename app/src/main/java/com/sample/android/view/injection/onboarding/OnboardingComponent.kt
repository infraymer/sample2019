package com.sample.android.view.injection.onboarding

import dagger.Subcomponent
import com.sample.android.view.onboarding.ui.fragment.LoginOnboardingFragment
import com.sample.android.view.onboarding.ui.fragment.OnboardingFragment

@Subcomponent
interface OnboardingComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): OnboardingComponent
    }

    fun inject(onboardingFragment: OnboardingFragment)
    fun inject(loginOnboardingFragment: LoginOnboardingFragment)
}