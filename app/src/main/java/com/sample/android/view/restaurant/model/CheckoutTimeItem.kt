package com.sample.android.view.restaurant.model

sealed class CheckoutTimeItem {
    class TimeItem(
        val time: String
    ) : CheckoutTimeItem()
}