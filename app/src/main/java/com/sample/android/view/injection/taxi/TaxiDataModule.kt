package com.sample.android.view.injection.taxi

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.taxi.repository.TaxiRepositoryImpl
import com.sample.android.data.taxi.source.TaxiRemoteDataSource
import com.sample.android.domain.taxi.repository.TaxiRepository
import com.sample.android.remote.taxi.TaxiRemoteDataSourceImpl
import com.sample.android.remote.taxi.service.TaxiService
import com.sample.android.view.injection.qualifiers.Navigator

@Module
internal abstract class TaxiDataModule {
    @Binds
    internal abstract fun bindTaxiRepository(
        repositoryImpl: TaxiRepositoryImpl
    ): TaxiRepository

    @Binds
    internal abstract fun bindTaxiRemoteDataSource(
        remoteDataSourceImpl: TaxiRemoteDataSourceImpl
    ): TaxiRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideTaxiService(@Navigator retrofit: Retrofit): TaxiService =
            retrofit.create(TaxiService::class.java)
    }
}