package com.sample.android.view.injection.event

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.event.EventPresenter

@Module
internal abstract class EventModule {
    @Binds
    @IntoMap
    @ViewModelKey(EventPresenter::class)
    internal abstract fun binPresenter(presenter: EventPresenter): ViewModel
}