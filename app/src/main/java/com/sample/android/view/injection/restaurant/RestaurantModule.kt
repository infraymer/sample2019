package com.sample.android.view.injection.restaurant

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.restaurant.RestaurantPresenter

@Module
internal abstract class RestaurantModule {
    @Binds
    @IntoMap
    @ViewModelKey(RestaurantPresenter::class)
    internal abstract fun binPresenter(presenter: RestaurantPresenter): ViewModel
}