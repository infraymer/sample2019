package com.sample.android.view.injection.base

interface ComponentHolder<out Component> {
    fun provideComponent(): Component
    fun releaseComponent()
}