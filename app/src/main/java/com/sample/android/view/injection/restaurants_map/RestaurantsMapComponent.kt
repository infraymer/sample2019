package com.sample.android.view.injection.restaurants_map

import dagger.Subcomponent
import com.sample.android.view.injection.restaurant_list.RestaurantListDataModule
import com.sample.android.view.injection.restaurant_rating.RestaurantRatingDataModule
import com.sample.android.view.injection.tag.TagsDataModule
import com.sample.android.view.reustarants_map.ui.fragment.RestaurantsMapFragment

@Subcomponent(
    modules = [
        RestaurantsMapModule::class,
        RestaurantListDataModule::class,
        RestaurantRatingDataModule::class,
        TagsDataModule::class
    ]
)
interface RestaurantsMapComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): RestaurantsMapComponent
    }

    fun inject(restaurantsMapFragment: RestaurantsMapFragment)
}