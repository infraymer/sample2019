package com.sample.android.view.injection.auth

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.cache.authorization.AuthorizationCacheDataSourceImpl
import com.sample.android.data.authorization.repository.AuthorizationRepositoryImpl
import com.sample.android.data.authorization.source.AuthorizationCacheDataSource
import com.sample.android.data.authorization.source.AuthorizationRemoteDataSource
import com.sample.android.domain.authorization.repository.AuthorizationRepository
import com.sample.android.remote.authorization.AuthorizationRemoteDataSourceImpl
import com.sample.android.remote.authorization.service.AuthorizationService
import com.sample.android.view.injection.qualifiers.Crm

@Module
internal abstract class AuthDataModule {
    @Binds
    internal abstract fun bindAuthorizationRepository(
        authorizationRepositoryImpl: AuthorizationRepositoryImpl
    ): AuthorizationRepository

    @Binds
    internal abstract fun bindAuthorizationRemoteDataSource(
        authRemoteDataSourceImpl: AuthorizationRemoteDataSourceImpl
    ): AuthorizationRemoteDataSource

    @Binds
    internal abstract fun bindAuthorizationCacheDataSource(
        authCacheDataSourceImpl: AuthorizationCacheDataSourceImpl
    ): AuthorizationCacheDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideAuthorizationService(@Crm retrofit: Retrofit): AuthorizationService =
            retrofit.create(AuthorizationService::class.java)
    }
}