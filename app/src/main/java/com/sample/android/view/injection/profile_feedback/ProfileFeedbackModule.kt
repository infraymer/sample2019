package com.sample.android.view.injection.profile_feedback

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.profile_feedback.ProfileFeedbackPresenter

@Module
internal abstract class ProfileFeedbackModule {
    @Binds
    @IntoMap
    @ViewModelKey(ProfileFeedbackPresenter::class)
    internal abstract fun bindPresenter(presenter: ProfileFeedbackPresenter): ViewModel
}