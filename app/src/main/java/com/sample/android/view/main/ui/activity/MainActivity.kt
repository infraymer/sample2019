package com.sample.android.view.main.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import com.sample.android.App
import com.sample.android.R
import com.sample.android.view.base.ui.listener.BackButtonListener
import com.sample.android.view.base.ui.listener.BackNavigable
import com.sample.android.view.profile_feedback.navigation.screen.ProfileFeedbackScreen
import com.sample.android.view.profile_pay_methods.navigation.screen.ProfilePayMethodsScreen
import javax.inject.Inject

class MainActivity : AppCompatActivity(), BackNavigable {

    @Inject
    internal lateinit var navigatorHolder: NavigatorHolder

    @Inject
    internal lateinit var router: Router

    private val navigator = SupportAppNavigator(this, supportFragmentManager, R.id.main_container)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectComponent()
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
//            router.newRootScreen(MainScreen())
            router.newRootScreen(ProfilePayMethodsScreen)
        }
    }

    private fun injectComponent() {
        App.component()
            .mainComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        (supportFragmentManager.findFragmentById(R.id.main_container) as? BackButtonListener)
            ?.onBackPressed()
            ?: router.exit()
    }

    override fun onNavigateBack() {
        router.exit()
    }
}
