package com.sample.android.view.injection.profile_pay_methods

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.profile_pay_methods.ProfilePayMethodsPresenter
import com.sample.android.presentation.base.injection.ViewModelKey

@Module
internal abstract class ProfilePayMethodsModule {
    @Binds
    @IntoMap
    @ViewModelKey(ProfilePayMethodsPresenter::class)
    internal abstract fun bindPresenter(presenter: ProfilePayMethodsPresenter): ViewModel
}