package com.sample.android.view.restaurant.ui.delegates

import android.view.View
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_restaurant_checkout.*

class GuestSetterViewDelegate(
    override val containerView: View
) : LayoutContainer {

    private var count: Int = 0
        set(value) {
            if (value < 0) return
            field = value
            numberGuestTextView.text = count.toString()
        }

    init {
        minusImageView.setOnClickListener { count-- }
        plusImageView.setOnClickListener { count++ }
        numberGuestTextView.text = count.toString()
    }
}