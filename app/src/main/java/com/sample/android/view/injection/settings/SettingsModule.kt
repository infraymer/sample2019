package com.sample.android.view.injection.settings

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.settings.SettingsPresenter

@Module
internal abstract class SettingsModule {

    @Binds
    @IntoMap
    @ViewModelKey(SettingsPresenter::class)
    internal abstract fun bindSettingsPresenter(settingsPresenter: SettingsPresenter): ViewModel
}