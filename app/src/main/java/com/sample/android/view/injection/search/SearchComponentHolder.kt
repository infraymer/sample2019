package com.sample.android.view.injection.search

import com.sample.android.view.injection.app.AppComponent
import com.sample.android.view.injection.base.SingleComponentHolder

class SearchComponentHolder(
    private val appComponent: AppComponent
) : SingleComponentHolder<SearchComponent>() {
    override fun provideInternal(): SearchComponent =
        appComponent
            .searchComponentBuilder()
            .build()
}