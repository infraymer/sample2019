package com.sample.android.view.profile_pay_methods.ui.adapter.delegates

import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import ru.nobird.android.ui.adapterdelegates.AdapterDelegate
import ru.nobird.android.ui.adapterdelegates.DelegateViewHolder
import com.sample.android.R
import com.sample.android.view.profile_pay_methods.model.PayMethodItem

class AddPayMethodAdapterDelegate(
    private val onClicked: () -> Unit = {}
) : AdapterDelegate<PayMethodItem, DelegateViewHolder<PayMethodItem>>() {
    override fun isForViewType(position: Int, data: PayMethodItem): Boolean =
        data is PayMethodItem.Add

    override fun onCreateViewHolder(parent: ViewGroup): DelegateViewHolder<PayMethodItem> =
        ViewHolder(createView(parent, R.layout.item_profile_pay_method_add))

    private inner class ViewHolder(
        override val containerView: View
    ) : DelegateViewHolder<PayMethodItem>(containerView), LayoutContainer {

        init {
            containerView.setOnClickListener { onClicked() }
        }
    }
}