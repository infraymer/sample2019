package com.sample.android.view.base.ui.extension

import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.sample.android.R

fun Fragment.snackbar(@StringRes messageRes: Int, length: Int = Snackbar.LENGTH_SHORT) {
    snackbar(getString(messageRes), length)
}

fun Fragment.snackbar(message: String, length: Int = Snackbar.LENGTH_SHORT) {
    val view = view
        ?: return

    Snackbar
        .make(view, message, length)
        .setTextColor(ContextCompat.getColor(view.context, R.color.white))
        .show()
}