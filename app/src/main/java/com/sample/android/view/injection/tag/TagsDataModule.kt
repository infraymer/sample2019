package com.sample.android.view.injection.tag

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.tag.repository.TagsRepositoryImpl
import com.sample.android.data.tag.source.TagsRemoteDataSource
import com.sample.android.domain.tag.repository.TagsRepository
import com.sample.android.remote.tags.TagsRemoteDataSourceImpl
import com.sample.android.remote.tags.serivce.TagsService
import com.sample.android.view.injection.qualifiers.Navigator

@Module
internal abstract class TagsDataModule {

    @Binds
    internal abstract fun bindTagsRepository(
        TagsRepositoryImpl: TagsRepositoryImpl
    ): TagsRepository

    @Binds
    internal abstract fun bindTagsRemoteDataSource(
        authRemoteDataSourceImpl: TagsRemoteDataSourceImpl
    ): TagsRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideTagsService(@Navigator retrofit: Retrofit): TagsService =
            retrofit.create(TagsService::class.java)
    }
}