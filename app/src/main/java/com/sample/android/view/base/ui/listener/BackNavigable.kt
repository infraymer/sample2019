package com.sample.android.view.base.ui.listener

interface BackNavigable {
    /**
     * Called when nested navigation cannot navigate back
     */
    fun onNavigateBack()
}