package com.sample.android.view.profile_edit.navigation.screen

import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import com.sample.android.view.profile_edit.ui.fragment.ProfileEditFragment

object ProfileEditScreen : SupportAppScreen() {

    override fun getFragment(): Fragment =
        ProfileEditFragment()
}