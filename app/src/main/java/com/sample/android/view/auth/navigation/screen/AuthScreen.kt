package com.sample.android.view.auth.navigation.screen

import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import com.sample.android.view.auth.ui.fragment.AuthFragment

class AuthScreen(
    private val isClosable: Boolean = true
) : SupportAppScreen() {
    override fun getFragment(): Fragment =
        AuthFragment.newInstance(isClosable)
}