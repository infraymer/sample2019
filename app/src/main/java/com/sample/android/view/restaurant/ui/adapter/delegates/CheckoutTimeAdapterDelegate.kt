package com.sample.android.view.restaurant.ui.adapter.delegates

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.nobird.android.ui.adapterdelegates.AdapterDelegate
import ru.nobird.android.ui.adapterdelegates.DelegateViewHolder
import ru.nobird.android.ui.adapters.selection.SelectionHelper
import com.sample.android.R
import com.sample.android.view.restaurant.model.CheckoutTimeItem

class CheckoutTimeAdapterDelegate(
    private val selectionHelper: SelectionHelper,
    private val onTimeItemClicked: (CheckoutTimeItem.TimeItem) -> Unit
) : AdapterDelegate<CheckoutTimeItem, DelegateViewHolder<CheckoutTimeItem>>() {

    override fun isForViewType(position: Int, data: CheckoutTimeItem): Boolean =
        data is CheckoutTimeItem.TimeItem

    override fun onCreateViewHolder(parent: ViewGroup): DelegateViewHolder<CheckoutTimeItem> =
        ViewHolder(createView(parent, R.layout.item_checkout_time))

    inner class ViewHolder(
        root: View
    ) : DelegateViewHolder<CheckoutTimeItem>(root) {
        init {
            root.setOnClickListener { (itemData as? CheckoutTimeItem.TimeItem)?.let(onTimeItemClicked) }
        }

        override fun onBind(data: CheckoutTimeItem) {
            data as CheckoutTimeItem.TimeItem
            itemView as TextView

            itemView.isSelected = selectionHelper.isSelected(adapterPosition)
            itemView.text = data.time
        }
    }
}