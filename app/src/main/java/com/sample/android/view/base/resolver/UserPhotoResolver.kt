package com.sample.android.view.base.resolver

interface UserPhotoResolver {

    fun getUserPhotoUrl(): String
}