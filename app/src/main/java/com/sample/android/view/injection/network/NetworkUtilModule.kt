package com.sample.android.view.injection.network

import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import com.sample.android.view.injection.qualifiers.StethoInterceptor
import com.sample.android.view.util.StethoHelper

@Module
abstract class NetworkUtilModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        @StethoInterceptor
        fun provideStethoInterceptor(): Interceptor =
            StethoHelper.getInterceptor()
    }
}