package com.sample.android.view.injection.auth

import dagger.Subcomponent
import com.sample.android.view.auth.ui.fragment.AuthFragment
import com.sample.android.view.injection.registration.RegistrationDataModule

@Subcomponent(
    modules = [
        AuthModule::class,
        AuthDataModule::class,
        RegistrationDataModule::class
    ]
)
interface AuthComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): AuthComponent
    }

    fun inject(authFragment: AuthFragment)
}