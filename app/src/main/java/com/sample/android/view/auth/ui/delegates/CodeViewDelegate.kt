package com.sample.android.view.auth.ui.delegates

import android.view.View
import androidx.core.text.buildSpannedString
import androidx.core.text.color
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_auth_code_from_sms.*
import com.sample.android.R
import com.sample.android.view.util.extension.getColorRes
import java.util.concurrent.TimeUnit

class CodeViewDelegate(
    override val containerView: View
) : LayoutContainer {
    companion object {
        const val TIMER = 5L
    }

    var isIncorrectCode: Boolean = false
        set(value) {
            field = value
            code_errorLabel.isVisible = value
        }

    var onLoginButtonClicked: (() -> Unit)? = null
    var onResetSmsButtonClicked: (() -> Unit)? = null
    var onCodeChanged: ((String) -> Unit)? = null

    private var timerObservable: Disposable? = null

    init {
        code_loginButton.setOnClickListener { onLoginButtonClicked?.invoke() }
        code_sendSmsTextView.setOnClickListener {
            onResetSmsButtonClicked?.invoke()
            resetTimer() // todo: manage it with presenter
        }
        code_codeEditText.doOnTextChanged { text, _, _, _ -> onCodeChanged?.invoke(text.toString()) }
    }

    private fun setCount(value: Long) {
        val greyColor = containerView.context.getColorRes(R.color.gray_text)
        val text = containerView.resources.getString(R.string.auth_resend_sms)
        code_sendSmsTextView.text = buildSpannedString {
            append(text)
            if (value > 0) {
                color(greyColor) { append(value.toString()) }
            }
        }
    }

    fun dispose() {
        timerObservable?.dispose()
    }

    fun resetTimer() {
        dispose()
        timerObservable = Observable
            .interval(1, TimeUnit.SECONDS)
            .take(TIMER)
            .map { TIMER - it - 1 }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                setCount(TIMER)
                code_sendSmsTextView.isEnabled = false
            }
            .doFinally {
                code_sendSmsTextView.isEnabled = true
            }
            .subscribeBy(onNext = ::setCount)
    }
}