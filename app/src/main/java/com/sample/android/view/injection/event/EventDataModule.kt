package com.sample.android.view.injection.event

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.event.repository.EventRepositoryImpl
import com.sample.android.data.event.source.EventRemoteDataSource
import com.sample.android.domain.event.repository.EventRepository
import com.sample.android.remote.event.EventRemoteDataSourceImpl
import com.sample.android.remote.event.service.EventService
import com.sample.android.view.injection.qualifiers.Navigator

@Module
internal abstract class EventDataModule {
    @Binds
    internal abstract fun bindEventRepository(
        repositoryImpl: EventRepositoryImpl
    ): EventRepository

    @Binds
    internal abstract fun bindEventRemoteDataSource(
        remoteDataSourceImpl: EventRemoteDataSourceImpl
    ): EventRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideEventInfoService(@Navigator retrofit: Retrofit): EventService =
            retrofit.create(EventService::class.java)
    }
}