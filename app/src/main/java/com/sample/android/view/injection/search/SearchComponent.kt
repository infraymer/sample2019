package com.sample.android.view.injection.search

import dagger.Subcomponent
import com.sample.android.view.injection.event_list.EventListDataModule
import com.sample.android.view.injection.restaurant_list.RestaurantListDataModule
import com.sample.android.view.injection.restaurant_rating.RestaurantRatingDataModule
import com.sample.android.view.injection.tag.TagsDataModule
import com.sample.android.view.search.ui.fragment.EventSearchResultFragment
import com.sample.android.view.search.ui.fragment.RestaurantSearchResultFragment
import com.sample.android.view.search.ui.fragment.SearchFragment

@SearchScope
@Subcomponent(modules = [
    SearchModule::class,

    EventListDataModule::class,
    RestaurantListDataModule::class,
    RestaurantRatingDataModule::class,

    TagsDataModule::class
])
interface SearchComponent {
    @Subcomponent.Builder
    interface Builder {
        fun build(): SearchComponent
    }

    fun inject(fragment: SearchFragment)
    fun inject(fragment: EventSearchResultFragment)
    fun inject(fragment: RestaurantSearchResultFragment)
}