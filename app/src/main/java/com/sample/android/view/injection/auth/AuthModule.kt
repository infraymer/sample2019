package com.sample.android.view.injection.auth

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.auth.AuthPresenter
import com.sample.android.presentation.base.injection.ViewModelKey

@Module
internal abstract class AuthModule {
    @Binds
    @IntoMap
    @ViewModelKey(AuthPresenter::class)
    internal abstract fun bindAuthPresenter(authPresenter: AuthPresenter): ViewModel
}