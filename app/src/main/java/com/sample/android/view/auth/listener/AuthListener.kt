package com.sample.android.view.auth.listener

interface AuthListener {

    fun onCloseScreen(isAuthorized: Boolean)
}