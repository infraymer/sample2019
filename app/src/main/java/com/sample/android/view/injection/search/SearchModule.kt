package com.sample.android.view.injection.search

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import io.reactivex.subjects.BehaviorSubject
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.search.EventSearchResultPresenter
import com.sample.android.presentation.search.RestaurantSearchResultPresenter
import com.sample.android.presentation.search.SearchPresenter
import com.sample.android.presentation.search.model.SearchConfiguration

@Module
internal abstract class SearchModule {
    @Binds
    @IntoMap
    @ViewModelKey(SearchPresenter::class)
    internal abstract fun bindSearchPresenter(presenter: SearchPresenter): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EventSearchResultPresenter::class)
    internal abstract fun bindEventSearchResultPresenter(presenter: EventSearchResultPresenter): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RestaurantSearchResultPresenter::class)
    internal abstract fun bindRestaurantSearchResultPresenter(presenter: RestaurantSearchResultPresenter): ViewModel


    @Module
    companion object {
        @Provides
        @JvmStatic
        @SearchScope
        fun provideSearchBus(): BehaviorSubject<SearchConfiguration> =
            BehaviorSubject.create()
    }
}