package com.sample.android.view.injection.profile_edit

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.profile_edit.ProfileEditPresenter

@Module
internal abstract class ProfileEditModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProfileEditPresenter::class)
    internal abstract fun bindProfileEditPresenter(profileEditPresenter: ProfileEditPresenter): ViewModel
}