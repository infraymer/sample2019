package com.sample.android.view.restaurant.ui.delegates

import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_restaurant_additional_info.*

class RestaurantAdditionalInfoDelegate(
    override val containerView: View,
    private val onLinkClicked: (url: String) -> Unit,
    private val onPhoneClicked: (phone: String) -> Unit
) : LayoutContainer {
    var link: String? = null
        set(value) {
            field = value
            restaurantLink.isVisible = value != null
        }

    var facebook: String? = null
        set(value) {
            field = value
            restaurantFb.isVisible = value != null
        }

    var instagram: String? = null
        set(value) {
            field = value
            restaurantInstagram.isVisible = value != null
        }

    var phone: String? = null
        set(value) {
            field = value
            restaurantPhone.isVisible = value != null
            restaurantPhone.text = value
        }

    init {
        restaurantLink.isGone = true
        restaurantFb.isGone = true
        restaurantInstagram.isGone = true
        restaurantPhone.isGone = true
        restaurantScheduleDays.isGone = true
        restaurantScheduleTime.isGone = true

        restaurantLink.setOnClickListener { link?.let(onLinkClicked) }
        restaurantFb.setOnClickListener { facebook?.let(onLinkClicked) }
        restaurantInstagram.setOnClickListener { instagram?.let(onLinkClicked) }

        restaurantPhone.setOnClickListener { phone?.let(onPhoneClicked) }
    }

    fun setSchedule(days: String?, time: String?) {
        restaurantScheduleDays.isVisible = days != null
        restaurantScheduleTime.isVisible = days != null && time != null

        restaurantScheduleDays.text = days
        restaurantScheduleTime.text = time
    }
}