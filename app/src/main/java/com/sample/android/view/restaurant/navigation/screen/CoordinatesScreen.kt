package com.sample.android.view.restaurant.navigation.screen

import android.content.Context
import android.content.Intent
import android.net.Uri
import ru.terrakok.cicerone.android.support.SupportAppScreen
import com.sample.android.domain.coordinates.model.Coordinates

class CoordinatesScreen(
    private val coordinates: Coordinates
) : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent =
        Intent(Intent.ACTION_VIEW, Uri.parse("geo:${coordinates.lat},${coordinates.lng}"))
}