package com.sample.android.view.auth.ui.delegates

import android.view.View
import androidx.core.view.isVisible
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_auth_sign_in.*
import com.sample.android.view.util.InputMask

class SignInViewDelegate(
    override val containerView: View
) : LayoutContainer {

    var isIncorrectPhone: Boolean = false
        set(value) {
            field = value
            signIn_errorLabel.isVisible = value
        }

    var onGetCodeButtonClicked: (() -> Unit)? = null
    var onPhoneChanged: ((String, Boolean) -> Unit)? = null

    init {
        MaskedTextChangedListener.installOn(
            signIn_phoneEditText,
            InputMask.PHONE,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(maskFilled: Boolean, extractedValue: String, formattedValue: String) {
                    onPhoneChanged?.invoke(extractedValue, maskFilled)
                }
            }
        )
        signIn_getCodeButton.setOnClickListener { onGetCodeButtonClicked?.invoke() }
    }
}