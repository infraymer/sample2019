package com.sample.android.view.base.mapper

import retrofit2.HttpException
import com.sample.android.domain.base.exception.ServerInternalError
import com.sample.android.domain.base.model.ErrorType
import java.net.HttpURLConnection

fun Throwable.toErrorType(): ErrorType =
    when (this) {
        is HttpException ->
            when (code()) {
                HttpURLConnection.HTTP_FORBIDDEN ->
                    ErrorType.FORBIDDEN

                HttpURLConnection.HTTP_UNAUTHORIZED ->
                    ErrorType.UNAUTHORIZED

                HttpURLConnection.HTTP_BAD_REQUEST ->
                    ErrorType.SERVER_ERROR

                else ->
                    ErrorType.NO_CONNECTION
            }

        is ServerInternalError ->
            ErrorType.INTERNAL_ERROR

        else ->
            ErrorType.NO_CONNECTION
    }