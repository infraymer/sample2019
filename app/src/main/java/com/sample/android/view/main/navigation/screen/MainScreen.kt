package com.sample.android.view.main.navigation.screen

import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import com.sample.android.view.main.ui.fragment.MainFragment

data class MainScreen(
    private val mainScreenTab: MainScreenTab = MainScreenTab.RESTAURANT_MAP
) : SupportAppScreen() {
    override fun getFragment(): Fragment =
        MainFragment.newInstance(mainScreenTab)
}