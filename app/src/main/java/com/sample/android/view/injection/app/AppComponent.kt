package com.sample.android.view.injection.app

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import com.sample.android.view.injection.auth.AuthComponent
import com.sample.android.view.injection.event.EventComponent
import com.sample.android.view.injection.event_list.EventListComponent
import com.sample.android.view.injection.main.MainComponent
import com.sample.android.view.injection.network.NetworkModule
import com.sample.android.view.injection.onboarding.OnboardingComponent
import com.sample.android.view.injection.profile.ProfileComponent
import com.sample.android.view.injection.profile_edit.ProfileEditComponent
import com.sample.android.view.injection.restaurant.RestaurantComponent
import com.sample.android.view.injection.profile_feedback.ProfileFeedbackComponent
import com.sample.android.view.injection.profile_pay_methods.ProfilePayMethodsComponent
import com.sample.android.view.injection.restaurants_map.RestaurantsMapComponent
import com.sample.android.view.injection.search.SearchComponent
import com.sample.android.view.injection.session.SessionModule
import com.sample.android.view.injection.settings.SettingsComponent

@ApplicationScope
@Component(
    modules = [
        AppModule::class,
        AppNavigationModule::class,
        NetworkModule::class,
        SessionModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        @BindsInstance
        fun context(context: Context): Builder
    }

    fun mainComponentBuilder(): MainComponent.Builder

    fun onboardingComponent(): OnboardingComponent.Builder

    fun restaurantsMapComponentBuilder(): RestaurantsMapComponent.Builder

    fun restaurantComponentBuilder(): RestaurantComponent.Builder

    fun eventComponentBuilder(): EventComponent.Builder

    fun eventsComponentBuilder(): EventListComponent.Builder

    fun profileComponentBuilder(): ProfileComponent.Builder

    fun authComponent(): AuthComponent.Builder

    fun profileEditComponent(): ProfileEditComponent.Builder

    fun searchComponentBuilder(): SearchComponent.Builder

    fun settingsComponentBuilder(): SettingsComponent.Builder

    fun profileFeedbackComponent(): ProfileFeedbackComponent.Builder

    fun profilePayMethodsComponent(): ProfilePayMethodsComponent.Builder
}