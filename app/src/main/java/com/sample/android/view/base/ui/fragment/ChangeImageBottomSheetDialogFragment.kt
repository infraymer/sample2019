package com.sample.android.view.base.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_change_avatar.*
import com.sample.android.R
import com.sample.android.view.base.ui.dialog.RoundedBottomSheetDialogFragment
import com.sample.android.view.base.ui.listener.ChangeImageListener

class ChangeImageBottomSheetDialogFragment : RoundedBottomSheetDialogFragment() {
    companion object {
        const val TAG = "ChangeImageBottomSheetDialogFragment"
        fun newInstance(position: Int): RoundedBottomSheetDialogFragment =
            ChangeImageBottomSheetDialogFragment().apply {
                this.position = position
            }
    }

    private var changeAvatarListener: ChangeImageListener? = null

    private var position: Int by argument()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.dialog_change_image, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changeAvatarListener = parentFragment as? ChangeImageListener

        replaceButton.setOnClickListener {
            changeAvatarListener?.onReplace(position)
            dismiss()
        }
        deleteButton.setOnClickListener {
            changeAvatarListener?.onDelete(position)
            dismiss()
        }
        cancelButton.setOnClickListener { dismiss() }
    }
}