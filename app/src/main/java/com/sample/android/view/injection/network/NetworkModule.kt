package com.sample.android.view.injection.network

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.sample.android.base.Config
import com.sample.android.remote.base.AuthInterceptor
import com.sample.android.remote.base.CrmAccessAppInterceptor
import com.sample.android.remote.base.NavigatorAccessAppInterceptor
import com.sample.android.remote.base.NavigatorAuthInterceptor
import com.sample.android.remote.base.serialization.UTCDateAdapter
import com.sample.android.view.injection.qualifiers.Crm
import com.sample.android.view.injection.qualifiers.Hostess
import com.sample.android.view.injection.qualifiers.Navigator
import com.sample.android.view.injection.qualifiers.StethoInterceptor
import java.util.Date

@Module(includes = [NetworkUtilModule::class])
abstract class NetworkModule {
    @Module
    companion object {
        @Provides
        @JvmStatic
        @Crm
        fun provideCrmRetrofit(
            config: Config,
            crmAuthInterceptor: AuthInterceptor,
            @StethoInterceptor
            stethoInterceptor: Interceptor,
            gsonConverterFactory: GsonConverterFactory
        ): Retrofit {
            val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(stethoInterceptor)
                .addInterceptor(CrmAccessAppInterceptor(config.crmAppToken))
                .addInterceptor(crmAuthInterceptor)
                .build()
            return Retrofit.Builder()
                .baseUrl(config.crmUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build()
        }

        @Provides
        @JvmStatic
        @Hostess
        fun provideHostessRetrofit(
            config: Config,
            @StethoInterceptor
            stethoInterceptor: Interceptor,
            gsonConverterFactory: GsonConverterFactory
        ): Retrofit {
            val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(stethoInterceptor)
                .addInterceptor(CrmAccessAppInterceptor(config.hostessAppToken))
                .build()
            return Retrofit.Builder()
                .baseUrl(config.hostessUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build()
        }

        @Provides
        @JvmStatic
        @Navigator
        fun provideNavigatorRetrofit(
            config: Config,
            navigatorAuthInterceptor: NavigatorAuthInterceptor,
            @StethoInterceptor
            stethoInterceptor: Interceptor,
            gsonConverterFactory: GsonConverterFactory
        ): Retrofit {
            val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(stethoInterceptor)
                .addInterceptor(NavigatorAccessAppInterceptor(config.navigatorAppToken))
                .addInterceptor(navigatorAuthInterceptor)
                .build()
            return Retrofit.Builder()
                .baseUrl(config.navigatorUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build()
        }

        @Provides
        @JvmStatic
        fun provideJsonFactory(): GsonConverterFactory =
            GsonBuilder()
                .registerTypeAdapter(Date::class.java, UTCDateAdapter())
                .create()
                .let(GsonConverterFactory::create)
    }
}