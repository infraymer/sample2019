package com.sample.android.view.injection.profile

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.sample.android.presentation.base.injection.ViewModelKey
import com.sample.android.presentation.profile.MainProfilePresenter
import com.sample.android.presentation.profile.ProfilePresenter

@Module
internal abstract class ProfileModule {
    @Binds
    @IntoMap
    @ViewModelKey(ProfilePresenter::class)
    internal abstract fun bindProfilePresenter(profilePresenter: ProfilePresenter): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainProfilePresenter::class)
    internal abstract fun bindMainProfilePresenter(mainProfilePresenter: MainProfilePresenter): ViewModel
}