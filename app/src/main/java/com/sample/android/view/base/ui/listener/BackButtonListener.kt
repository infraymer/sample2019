package com.sample.android.view.base.ui.listener

interface BackButtonListener {
    fun onBackPressed(): Boolean
}