package com.sample.android.view.base.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import com.sample.android.R

class LoadingProgressDialog
constructor(
    context: Context
) : Dialog(context, R.style.ProgressDialog) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_progress)
        /*progressDialogTitle.setText(titleRes)
        progressDialogMessage.setText(R.string.loading_message)*/
        setCanceledOnTouchOutside(false)
        setCancelable(false)
    }
}