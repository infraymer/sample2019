package com.sample.android.view.injection.restaurant_rating

import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import com.sample.android.data.restaurant_rating.repository.RestaurantRatingRepositoryImpl
import com.sample.android.data.restaurant_rating.source.RestaurantRatingRemoteDataSource
import com.sample.android.domain.restaurant_rating.repository.RestaurantRatingRepository
import com.sample.android.remote.restaurant_rating.RestaurantRatingRemoteDataSourceImpl
import com.sample.android.remote.restaurant_rating.service.RestaurantRatingService
import com.sample.android.view.injection.qualifiers.Crm

@Module
internal abstract class RestaurantRatingDataModule {
    @Binds
    internal abstract fun bindRestaurantRatingRepository(
        repositoryImpl: RestaurantRatingRepositoryImpl
    ): RestaurantRatingRepository

    @Binds
    internal abstract fun bindRestaurantRatingRemoteDataSource(
        remoteDataSourceImpl: RestaurantRatingRemoteDataSourceImpl
    ): RestaurantRatingRemoteDataSource

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideService(@Crm retrofit: Retrofit): RestaurantRatingService =
            retrofit.create(RestaurantRatingService::class.java)
    }
}