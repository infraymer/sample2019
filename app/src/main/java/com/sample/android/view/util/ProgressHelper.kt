package com.sample.android.view.util

import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.sample.android.view.base.ui.dialog.LoadingProgressDialog

object ProgressHelper {

    fun dismiss(mProgressLogin: ProgressBar) {
        if (mProgressLogin.visibility != View.GONE) {
            mProgressLogin.visibility = View.GONE
        }
    }

    fun activate(progressBar: ProgressBar) {
        progressBar.visibility = View.VISIBLE
    }

    fun dismiss(swipeRefreshLayout: SwipeRefreshLayout) {
        swipeRefreshLayout.isRefreshing = false
    }

    fun activate(swipeRefreshLayout: SwipeRefreshLayout) {
        swipeRefreshLayout.setRefreshing(true)
    }

    fun activate(progressDialog: LoadingProgressDialog) {
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    fun dismiss(progressDialog: LoadingProgressDialog) {
        if (progressDialog.isShowing) {
            try {
                progressDialog.dismiss()
            } catch (ignored: Exception) {
            }
        }
    }

    fun activate(progressDialog: DialogFragment, fragmentManager: FragmentManager?, tag: String) {
        if (!progressDialog.isAdded && fragmentManager?.findFragmentByTag(tag) == null) {
            progressDialog.show(fragmentManager, tag)
        }
    }

    fun dismiss(fragmentManager: FragmentManager?, tag: String) {
        try {
            val fragment = fragmentManager?.findFragmentByTag(tag) as DialogFragment
            fragment.dismiss()
        } catch (ignored: Exception) {
        }
    }
}