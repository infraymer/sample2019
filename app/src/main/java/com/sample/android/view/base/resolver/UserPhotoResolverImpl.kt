package com.sample.android.view.base.resolver

import com.sample.android.base.Config
import javax.inject.Inject

class UserPhotoResolverImpl
@Inject
constructor(
    private val config: Config
) : UserPhotoResolver {

    override fun getUserPhotoUrl(): String =
        config.crmUrl + "crm/photo"
}