package com.sample.android.data.restaurant_rating.source

import io.reactivex.Maybe
import com.sample.android.domain.restaurant_rating.model.RestaurantRating

interface RestaurantRatingRemoteDataSource {
    fun getRestaurantRating(primepassId: String): Maybe<RestaurantRating>
}