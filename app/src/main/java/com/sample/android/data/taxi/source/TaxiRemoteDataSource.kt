package com.sample.android.data.taxi.source

import io.reactivex.Single
import com.sample.android.domain.coordinates.model.Coordinates
import com.sample.android.domain.taxi.model.Taxi

interface TaxiRemoteDataSource {
    fun getTaxi(startCoordinates: Coordinates, endCoordinates: Coordinates): Single<List<Taxi>>
}