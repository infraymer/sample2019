package com.sample.android.data.authorization.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import com.sample.android.data.authorization.source.AuthorizationCacheDataSource
import com.sample.android.data.authorization.source.AuthorizationRemoteDataSource
import com.sample.android.domain.authorization.model.AuthData
import com.sample.android.domain.authorization.model.Authorization
import com.sample.android.domain.authorization.repository.AuthorizationRepository
import com.sample.android.domain.base.util.doCompletableOnSuccess
import javax.inject.Inject

class AuthorizationRepositoryImpl
@Inject
constructor(
    private val authorizationRemoteDataSource: AuthorizationRemoteDataSource,
    private val authorizationCacheDataSource: AuthorizationCacheDataSource
) : AuthorizationRepository {

    override fun authorizationGetSms(authorization: Authorization): Completable =
        authorizationRemoteDataSource.authorizationGetSms(authorization)

    override fun authorizationConfirm(authorization: Authorization): Single<AuthData> =
        authorizationRemoteDataSource.authorizationConfirm(authorization)
            .doCompletableOnSuccess(authorizationCacheDataSource::setAuthData)

    override fun recoveryPassword(): Completable =
        authorizationRemoteDataSource.recoveryPassword()

    override fun changePassword(): Completable =
        authorizationRemoteDataSource.changePassword()

    override fun confirmRecoveryPassword(): Completable =
        authorizationRemoteDataSource.confirmRecoveryPassword()

    override fun getAuthData(): Maybe<AuthData> =
        authorizationCacheDataSource.getAuthData()
}