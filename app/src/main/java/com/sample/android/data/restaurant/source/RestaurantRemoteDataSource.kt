package com.sample.android.data.restaurant.source

import io.reactivex.Single
import com.sample.android.domain.restaurant.model.RestaurantInfo

interface RestaurantRemoteDataSource {
    fun getRestaurantInfo(restaurantId: String): Single<RestaurantInfo>
}