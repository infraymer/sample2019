package com.sample.android.data.authorization.source

import io.reactivex.Completable
import io.reactivex.Maybe
import com.sample.android.domain.authorization.model.AuthData

interface AuthorizationCacheDataSource {

    fun setAuthData(data: AuthData): Completable

    fun getAuthData(): Maybe<AuthData>
}