package com.sample.android.data.authorization.source

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.domain.authorization.model.AuthData
import com.sample.android.domain.authorization.model.Authorization

interface AuthorizationRemoteDataSource {

    fun authorizationGetSms(authorization: Authorization): Completable

    fun authorizationConfirm(authorization: Authorization): Single<AuthData>

    fun recoveryPassword(): Completable

    fun changePassword(): Completable

    fun confirmRecoveryPassword(): Completable
}