package com.sample.android.data.restaurant_rating.repository

import io.reactivex.Maybe
import com.sample.android.data.restaurant_rating.source.RestaurantRatingRemoteDataSource
import com.sample.android.domain.restaurant_rating.model.RestaurantRating
import com.sample.android.domain.restaurant_rating.repository.RestaurantRatingRepository
import javax.inject.Inject

class RestaurantRatingRepositoryImpl
@Inject
constructor(
    private val restaurantRatingRemoteDataSource: RestaurantRatingRemoteDataSource
) : RestaurantRatingRepository {
    override fun getRestaurantRating(primepassId: String): Maybe<RestaurantRating> =
        restaurantRatingRemoteDataSource
            .getRestaurantRating(primepassId)
}