package com.sample.android.data.session.repository

import io.reactivex.Single
import com.sample.android.data.session.source.SessionCacheDataSource
import com.sample.android.domain.session.repository.SessionRepository
import javax.inject.Inject

class SessionRepositoryImpl
@Inject
constructor(
    private val sessionCacheDataSource: SessionCacheDataSource
) : SessionRepository {

    override fun isFirstSession(): Single<Boolean> =
        sessionCacheDataSource.isFirstSession()
}