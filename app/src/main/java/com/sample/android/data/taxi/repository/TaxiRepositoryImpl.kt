package com.sample.android.data.taxi.repository

import io.reactivex.Single
import com.sample.android.data.taxi.source.TaxiRemoteDataSource
import com.sample.android.domain.coordinates.model.Coordinates
import com.sample.android.domain.taxi.model.Taxi
import com.sample.android.domain.taxi.repository.TaxiRepository
import javax.inject.Inject

class TaxiRepositoryImpl
@Inject
constructor(
    private val taxiRemoteDataSource: TaxiRemoteDataSource
) : TaxiRepository {
    override fun getTaxi(startCoordinates: Coordinates, endCoordinates: Coordinates): Single<List<Taxi>> =
        taxiRemoteDataSource
            .getTaxi(startCoordinates, endCoordinates)
}