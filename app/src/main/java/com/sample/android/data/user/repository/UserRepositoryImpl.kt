package com.sample.android.data.user.repository

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.data.user.source.UserRemoteDataSource
import com.sample.android.domain.user.model.User
import com.sample.android.domain.user.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl
@Inject
constructor(
    private val userRemoteDataSource: UserRemoteDataSource
) : UserRepository {

    override fun getUser(userId: Long): Single<User> =
        userRemoteDataSource.getUser(userId)

    override fun updateUser(user: User): Completable =
        userRemoteDataSource.updateUser(user)

    override fun getUserCardInfo(): Single<Unit> =
        userRemoteDataSource.getUserCardInfo()
}