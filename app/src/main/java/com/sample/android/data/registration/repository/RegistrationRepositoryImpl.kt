package com.sample.android.data.registration.repository

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.data.registration.source.RegistrationRemoteDataSource
import com.sample.android.domain.registration.model.Registration
import com.sample.android.domain.registration.repository.RegistrationRepository
import com.sample.android.domain.user.model.User
import javax.inject.Inject

class RegistrationRepositoryImpl
@Inject
constructor(
    private val registrationRemoteDataSource: RegistrationRemoteDataSource
) : RegistrationRepository {

    override fun createUser(registration: Registration): Single<User> =
        registrationRemoteDataSource.createUser(registration)

    override fun confirmRegistration(): Completable =
        registrationRemoteDataSource.confirmRegistration()
}