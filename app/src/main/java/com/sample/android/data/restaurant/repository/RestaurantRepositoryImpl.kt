package com.sample.android.data.restaurant.repository

import io.reactivex.Single
import com.sample.android.data.restaurant.source.RestaurantRemoteDataSource
import com.sample.android.domain.restaurant.model.RestaurantInfo
import com.sample.android.domain.restaurant.repository.RestaurantRepository
import javax.inject.Inject

class RestaurantRepositoryImpl
@Inject
constructor(
    private val restaurantRemoteDataSource: RestaurantRemoteDataSource
) : RestaurantRepository {
    override fun getRestaurantInfo(restaurantId: String): Single<RestaurantInfo> =
        restaurantRemoteDataSource
            .getRestaurantInfo(restaurantId)
}