package com.sample.android.data.session.source

import io.reactivex.Single

interface SessionCacheDataSource {

    fun isFirstSession(): Single<Boolean>
}