package com.sample.android.data.user.source

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.domain.user.model.User

interface UserRemoteDataSource {

    fun getUser(userId: Long): Single<User>

    fun updateUser(user: User): Completable

    fun getUserCardInfo(): Single<Unit>
}