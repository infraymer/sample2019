package com.sample.android.presentation.profile

import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import com.sample.android.domain.authorization.interactor.AuthorizationInteractor
import com.sample.android.presentation.base.PresenterBase
import com.sample.android.view.injection.qualifiers.BackgroundScheduler
import com.sample.android.view.injection.qualifiers.MainScheduler
import com.sample.android.view.settings.navigation.screen.SettingsScreen
import javax.inject.Inject

class MainProfilePresenter
@Inject
constructor(
    private val router: Router,

    private val authorizationInteractor: AuthorizationInteractor,

    @BackgroundScheduler
    private val backgroundScheduler: Scheduler,
    @MainScheduler
    private val mainScheduler: Scheduler
) : PresenterBase<MainProfileView>() {

    private var state: MainProfileView.State = MainProfileView.State.Auth
        set(value) {
            field = value
            view?.setState(value)
        }

    init {
        compositeDisposable += authorizationInteractor
            .getAuthData()
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onSuccess = { state = MainProfileView.State.Profile },
                onComplete = { state = MainProfileView.State.Auth },
                onError = {}
            )
    }

    fun onAuthorization(isAuthorized: Boolean) {
        if (isAuthorized)
            state = MainProfileView.State.Profile
        else
            view?.back()
    }

    fun onSettingsClicked() {
        router.navigateTo(SettingsScreen)
    }

    fun onBackPressed() {
        view?.back()
    }
}