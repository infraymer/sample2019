package com.sample.android.presentation.profile

import ru.terrakok.cicerone.Router
import com.sample.android.presentation.base.PresenterBase
import com.sample.android.view.settings.navigation.screen.SettingsScreen
import javax.inject.Inject

class ProfilePresenter
@Inject
constructor(
    private val router: Router
) : PresenterBase<ProfileView>() {
    fun onShowSettingsClicked() {
        router.navigateTo(SettingsScreen)
    }
}