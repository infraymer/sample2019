package com.sample.android.presentation.paged_list

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import com.sample.android.domain.base.model.PagedList
import com.sample.android.domain.base.model.concatWithPagedList
import com.sample.android.presentation.base.PresenterBase

abstract class PagedListPresenter<T, V : PagedListView<T>>(
    private val backgroundScheduler: Scheduler,
    private val mainScheduler: Scheduler
) : PresenterBase<V>() {
    private var state: PagedListView.State<T> = PagedListView.State.Idle()
        set(value) {
            field = value
            view?.setState(value)
        }

    private val paginationDisposable = CompositeDisposable()

    init {
        compositeDisposable += paginationDisposable
    }

    override fun attachView(view: V) {
        super.attachView(view)
        view.setState(state)
    }

    fun fetchItems() {
        if (state !is PagedListView.State.Idle<T>) return

        state = PagedListView.State.Loading()
        paginationDisposable += getItems(page = 1)
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onSuccess = { items ->
                    state = if (items.isEmpty()) {
                        PagedListView.State.EmptyContent()
                    } else {
                        PagedListView.State.Remote(items)
                    }
                },
                onError = { state = PagedListView.State.NetworkError() }
            )
    }

    fun fetchNextPageFromRemote() {
        val oldState = state

        val oldItems = (oldState as? PagedListView.State.Remote<T>)
            ?.items
            ?.takeIf { it.hasNext }
            ?: return

        val nextPage = oldItems.page + 1

        state = PagedListView.State.RemoteLoading(oldItems)
        paginationDisposable += getItems(page = nextPage)
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onSuccess = { state = PagedListView.State.Remote(oldItems.concatWithPagedList(it)) },
                onError = { state = oldState; view?.showNetworkError() }
            )
    }

    /**
     * Force updates current list
     * If [preserveOldStateInCaseOfError] is true old state will be preserved, state will be [PagedListView.State.NetworkError] otherwise
     */
    fun forceUpdate(preserveOldStateInCaseOfError: Boolean = true) {
        paginationDisposable.clear()

        val oldState = state

        state = PagedListView.State.Loading()
        paginationDisposable += getItems(page = 1)
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onSuccess = { items ->
                    state =
                        if (items.isEmpty()) {
                            PagedListView.State.EmptyContent()
                        } else {
                            PagedListView.State.Remote(items)
                        }
                },
                onError = {
                    if (preserveOldStateInCaseOfError) {
                        when (oldState) {
                            is PagedListView.State.Remote<T> -> {
                                state = oldState
                                view?.showNetworkError()
                            }

                            is PagedListView.State.RemoteLoading<T> ->
                                state = PagedListView.State.Remote(oldState.items)

                            else ->
                                state = PagedListView.State.NetworkError()
                        }
                    } else {
                        state = PagedListView.State.NetworkError()
                    }
                }
            )
    }

    protected abstract fun getItems(page: Int): Single<PagedList<T>>
}