package com.sample.android.presentation.restaurant

import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import com.sample.android.domain.coordinates.model.Coordinates
import com.sample.android.domain.restaurant.interactor.RestaurantInteractor
import com.sample.android.domain.restaurant_map.model.RestaurantListItem
import com.sample.android.presentation.base.PresenterBase
import com.sample.android.view.base.navigation.screen.LinkScreen
import com.sample.android.view.base.navigation.screen.PhoneScreen
import com.sample.android.view.injection.qualifiers.BackgroundScheduler
import com.sample.android.view.injection.qualifiers.MainScheduler
import com.sample.android.view.restaurant.navigation.screen.CoordinatesScreen
import javax.inject.Inject

class RestaurantPresenter
@Inject
constructor(
    private val restaurantInteractor: RestaurantInteractor,

    private val router: Router,

    @BackgroundScheduler
    private val backgroundScheduler: Scheduler,
    @MainScheduler
    private val mainScheduler: Scheduler
) : PresenterBase<RestaurantView>() {
    private var state: RestaurantView.State = RestaurantView.State.Idle
        set(value) {
            field = value
            view?.setState(value)
        }

    override fun attachView(view: RestaurantView) {
        super.attachView(view)
        view.setState(state)
    }

    fun onRestaurantListItem(restaurantListItem: RestaurantListItem, forceUpdate: Boolean = false) {
        if (state != RestaurantView.State.Idle &&
            !(state == RestaurantView.State.NetworkError && forceUpdate)) {
            return
        }

        state = RestaurantView.State.Loading
        compositeDisposable += restaurantInteractor
            .getRestaurant(restaurantListItem)
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onSuccess = { state = RestaurantView.State.Remote(it) },
                onError = { state = RestaurantView.State.NetworkError }
            )
    }

    fun onLinkClicked(url: String) {
        router.navigateTo(LinkScreen(url))
    }

    fun onPhoneClicked(phone: String) {
        router.navigateTo(PhoneScreen(phone))
    }

    fun onCoordinatesClicked(coordinates: Coordinates) {
        router.navigateTo(CoordinatesScreen(coordinates))
    }
}