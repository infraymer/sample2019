package com.sample.android.presentation.auth

import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import com.sample.android.domain.authorization.interactor.AuthorizationInteractor
import com.sample.android.domain.authorization.model.Authorization
import com.sample.android.domain.base.model.ErrorType
import com.sample.android.domain.registration.interactor.RegistrationInteractor
import com.sample.android.domain.registration.model.Registration
import com.sample.android.presentation.base.PresenterBase
import com.sample.android.view.base.mapper.toErrorType
import com.sample.android.view.injection.qualifiers.BackgroundScheduler
import com.sample.android.view.injection.qualifiers.MainScheduler
import javax.inject.Inject

class AuthPresenter
@Inject
constructor(
    private val authInteractor: AuthorizationInteractor,
    private val registrationInteractor: RegistrationInteractor,

    @BackgroundScheduler
    private val backgroundScheduler: Scheduler,
    @MainScheduler
    private val mainScheduler: Scheduler
) : PresenterBase<AuthView>() {

    private var state: AuthView.State = AuthView.State.SignIn
        set(value) {
            lastState = field
            field = value
            view?.setState(value)
        }

    private var lastState: AuthView.State = AuthView.State.SignIn

    var phoneAuth: String = ""
        set(value) {
            field = "8$value"
        }
    var phoneReg: String = ""
        set(value) {
            field = "8$value"
        }
    var code: String = ""
    var phoneAuthFilled: Boolean = false
    var phoneRegFilled: Boolean = false
    var firstName: String = ""
    var lastName: String = ""

    override fun attachView(view: AuthView) {
        super.attachView(view)
        view.setState(state)
    }

    private fun getCodeSms(phone: String) {
        compositeDisposable += authInteractor
            .authorizationGetSms(Authorization(phone))
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onComplete = { state = AuthView.State.Code },
                onError = {
                    val errorType = it.toErrorType()
                    if (errorType == ErrorType.INTERNAL_ERROR) {
                        view?.showMessage(it.message ?: "")
                    }
                }
            )
    }

    fun onGetCodeClicked() {
        getCodeSms(phoneAuth)
    }

    fun onSignInClicked() {
        compositeDisposable += authInteractor
            .authorizationConfirm(Authorization(phoneAuth, password = code))
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onSuccess = { view?.close(true) },
                onError = {
                    val errorType = it.toErrorType()
                    if (errorType == ErrorType.INTERNAL_ERROR) {
                        view?.showMessage(it.message ?: "")
                    }
                }
            )
    }

    fun onSignUpClicked() {
        val registration = Registration(phoneReg, firstName, lastName)
        compositeDisposable += registrationInteractor
            .register(registration)
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(
                onSuccess = { getCodeSms(phoneReg) },
                onError = {
                    val errorType = it.toErrorType()
                    if (errorType == ErrorType.INTERNAL_ERROR) {
                        view?.showMessage(it.message ?: "")
                    }
                }
            )
    }

    fun onResendSmsClicked() {
        getCodeSms(
            if (lastState is AuthView.State.SignIn)
                phoneAuth
            else
                phoneReg
        )
    }

    fun onSignInTabClicked() {
        state = AuthView.State.SignIn
    }

    fun onSignUpTabClicked() {
        state = AuthView.State.SignUp
    }

    fun onBackPressed() {
        if (state is AuthView.State.Code)
            state = lastState
        else
            view?.close(false)
    }
}