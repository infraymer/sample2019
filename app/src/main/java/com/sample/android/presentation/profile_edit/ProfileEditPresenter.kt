package com.sample.android.presentation.profile_edit

import android.graphics.Bitmap
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import com.sample.android.domain.user.interactor.UserInteractor
import com.sample.android.domain.user.model.User
import com.sample.android.presentation.base.PresenterBase
import com.sample.android.view.injection.qualifiers.BackgroundScheduler
import com.sample.android.view.injection.qualifiers.MainScheduler
import javax.inject.Inject

class ProfileEditPresenter
@Inject
constructor(
    private val router: Router,

    private val userInteractor: UserInteractor,

    @BackgroundScheduler
    private val backgroundScheduler: Scheduler,
    @MainScheduler
    private val mainScheduler: Scheduler
) : PresenterBase<ProfileEditView>() {

    private var state: ProfileEditView.State = ProfileEditView.State.Idle
        set(value) {
            field = value
            view?.setState(value)
        }

    private var userId = 0L

    init {
        compositeDisposable += userInteractor
            .getUser()
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .doOnSubscribe { state = ProfileEditView.State.Loading }
            .subscribeBy(
                onSuccess = {
                    userId = it.id
                    state = ProfileEditView.State.Loaded(it)
                },
                onError = {
                    state = ProfileEditView.State.Idle
                    view?.showMessage(it.message ?: "")
                    router.exit()
                }
            )
    }

    override fun attachView(view: ProfileEditView) {
        super.attachView(view)
        view.setState(state)
    }

    private fun updateAvatar(bitmap: Bitmap?) {
        val oldState = (state as? ProfileEditView.State.Loaded)
            ?: return
        state = ProfileEditView.State.Loaded(oldState.data.copy(avatar = bitmap))
    }

    fun saveUserData(user: User) {
        val oldState = (state as? ProfileEditView.State.Loaded)
            ?: return
        state = ProfileEditView.State.Loaded(
            user.copy(
                avatar = oldState.data.avatar
            )
        )
    }

    fun onImageBitmap(bitmap: Bitmap?) {
        updateAvatar(bitmap)
    }

    fun onDeleteAvatarClicked() {
        updateAvatar(null)
    }

    fun onReplaceAvatarClicked() {
        view?.showImagePicker()
    }

    fun onDoneClicked(data: User) {
        val newUser = data.copy(id = userId)
        compositeDisposable += userInteractor
            .updateUser(newUser)
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .doOnSubscribe { state = ProfileEditView.State.Loading }
            .subscribeBy(
                onComplete = {
                    state = ProfileEditView.State.Complete
                    router.exit()
                },
                onError = {
                    state = ProfileEditView.State.Loaded(newUser)
                    view?.showMessage(it.message ?: "")
                }
            )
    }
}