package com.sample.android.presentation.main

import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.terrakok.cicerone.Router
import com.sample.android.domain.session.interactor.SessionInteractor
import com.sample.android.presentation.base.PresenterBase
import com.sample.android.view.injection.qualifiers.BackgroundScheduler
import com.sample.android.view.injection.qualifiers.MainScheduler
import com.sample.android.view.onboarding.navigation.screen.OnboardingScreen
import javax.inject.Inject

class MainPresenter
@Inject
constructor(
    sessionInteractor: SessionInteractor,

    private val router: Router,

    @BackgroundScheduler
    private val backgroundScheduler: Scheduler,
    @MainScheduler
    private val mainScheduler: Scheduler
) : PresenterBase<MainView>() {

    init {
        compositeDisposable += sessionInteractor
            .isFirstSession()
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .filter { it }
            .subscribeBy { router.navigateTo(OnboardingScreen) }
    }
}