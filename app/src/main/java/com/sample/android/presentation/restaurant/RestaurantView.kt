package com.sample.android.presentation.restaurant

import com.sample.android.domain.restaurant.model.Restaurant

interface RestaurantView {
    sealed class State {
        object Idle : State()
        object Loading : State()
        object NetworkError : State()

        data class Remote(val restaurant: Restaurant) : State()
    }

    fun setState(state: State)
}