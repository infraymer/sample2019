package com.sample.android.presentation.auth

interface AuthView {
    sealed class State {

        object SignIn : State()
        object SignUp : State()
        object Code : State()
    }

    fun setState(state: State)
    fun showMessage(text: String)
    fun close(isAuthorized: Boolean)
}