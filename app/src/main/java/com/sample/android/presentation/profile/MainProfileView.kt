package com.sample.android.presentation.profile

interface MainProfileView {

    sealed class State {

        object Auth : State()

        object Profile : State()
    }

    fun setState(state: State)
    fun back()
}