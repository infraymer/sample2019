package com.sample.android.presentation.profile_edit

import com.sample.android.domain.user.model.User

interface ProfileEditView {

    sealed class State {

        object Idle : State()
        object Loading : State()
        object Complete : State()

        data class Loaded(
            val data: User
        ) : State()
    }

    fun setState(state: State)
    fun showImagePicker()
    fun showMessage(text: String)
}