package com.sample.android.presentation.paged_list

import com.sample.android.domain.base.model.PagedList

interface PagedListView<T> {
    sealed class State<T> {
        class Idle<T> : State<T>()
        class Loading<T> : State<T>()
        class EmptyContent<T> : State<T>()
        class NetworkError<T> : State<T>()

        class Remote<T>(val items: PagedList<T>) : State<T>()
        class RemoteLoading<T>(val items: PagedList<T>) : State<T>()
    }

    fun setState(state: State<T>)
    fun showNetworkError()
}