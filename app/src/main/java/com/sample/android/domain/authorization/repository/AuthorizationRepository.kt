package com.sample.android.domain.authorization.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import com.sample.android.domain.authorization.model.AuthData
import com.sample.android.domain.authorization.model.Authorization

interface AuthorizationRepository {

    fun authorizationGetSms(authorization: Authorization): Completable

    fun authorizationConfirm(authorization: Authorization): Single<AuthData>

    fun recoveryPassword(): Completable

    fun changePassword(): Completable

    fun confirmRecoveryPassword(): Completable

    fun getAuthData(): Maybe<AuthData>
}