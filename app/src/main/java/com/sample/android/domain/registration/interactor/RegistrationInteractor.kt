package com.sample.android.domain.registration.interactor

import io.reactivex.Single
import com.sample.android.domain.registration.model.Registration
import com.sample.android.domain.registration.repository.RegistrationRepository
import com.sample.android.domain.user.model.User
import javax.inject.Inject

class RegistrationInteractor
@Inject
constructor(
    private val registrationRepository: RegistrationRepository
) {

    fun register(registration: Registration): Single<User> =
        registrationRepository.createUser(registration)
}