package com.sample.android.domain.user.interactor

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.domain.authorization.repository.AuthorizationRepository
import com.sample.android.domain.user.model.User
import com.sample.android.domain.user.repository.UserRepository
import javax.inject.Inject

class UserInteractor
@Inject
constructor(
    private val userRepository: UserRepository,
    private val authorizationRepository: AuthorizationRepository
) {

    fun getUser(): Single<User> =
        authorizationRepository
            .getAuthData()
            .flatMapSingle { userRepository.getUser(it.userId) }

    fun updateUser(user: User): Completable =
        userRepository.updateUser(user)
}