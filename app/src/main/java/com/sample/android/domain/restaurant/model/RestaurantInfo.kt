package com.sample.android.domain.restaurant.model

import com.sample.android.domain.media.model.Media

data class RestaurantInfo(
    val description: String?,
    val phone: String?,
    val site: String?,

    val images360: List<Media>,
    val images360small: List<Media>
)