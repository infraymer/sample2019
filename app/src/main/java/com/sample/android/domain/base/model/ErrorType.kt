package com.sample.android.domain.base.model

enum class ErrorType {
    NO_CONNECTION,
    FORBIDDEN,
    UNAUTHORIZED,
    SERVER_ERROR,
    INTERNAL_ERROR
}