package com.sample.android.domain.paged_list.model

import java.util.Date

class NavigatorListQuery(
    val page: Int = 1,
    val pageSize: Int = 10,

    val id: String? = null,
    val tag: String? = null,

    val eventId: String? = null,
    val restaurantId: String? = null,

    val titleSelector: String? = null,
    val dateSelector: Date? = null
)