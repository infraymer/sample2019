package com.sample.android.domain.restaurant_rating.repository

import io.reactivex.Maybe
import com.sample.android.domain.restaurant_rating.model.RestaurantRating

interface RestaurantRatingRepository {
    fun getRestaurantRating(primepassId: String): Maybe<RestaurantRating>
}