package com.sample.android.domain.comment.model

import com.google.gson.annotations.SerializedName
import java.util.Date

class Comment(
    @SerializedName("place")
    val primepassId: String,

    @SerializedName("review")
    val review: String,

    @SerializedName("assessment")
    val assessment: Float,

    @SerializedName("user_id")
    val userId: Long,

    @SerializedName("time_key")
    val time: Date
)