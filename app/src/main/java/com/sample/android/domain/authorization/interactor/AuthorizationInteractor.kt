package com.sample.android.domain.authorization.interactor

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import com.sample.android.domain.authorization.model.AuthData
import com.sample.android.domain.authorization.model.Authorization
import com.sample.android.domain.authorization.repository.AuthorizationRepository
import javax.inject.Inject

class AuthorizationInteractor
@Inject
constructor(
    private val authRepository: AuthorizationRepository
) {

    fun authorizationGetSms(authorization: Authorization): Completable =
        authRepository.authorizationGetSms(authorization)

    fun authorizationConfirm(authorization: Authorization): Single<AuthData> =
        authRepository.authorizationConfirm(authorization)

    fun getAuthData(): Maybe<AuthData> =
        authRepository.getAuthData()
}