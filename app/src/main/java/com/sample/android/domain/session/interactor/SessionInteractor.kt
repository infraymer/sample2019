package com.sample.android.domain.session.interactor

import io.reactivex.Single
import com.sample.android.domain.session.repository.SessionRepository
import javax.inject.Inject

class SessionInteractor
@Inject
constructor(
    private val sessionRepository: SessionRepository
) {
    fun isFirstSession(): Single<Boolean> =
        sessionRepository.isFirstSession()
}