package com.sample.android.domain.taxi.repository

import io.reactivex.Single
import com.sample.android.domain.coordinates.model.Coordinates
import com.sample.android.domain.taxi.model.Taxi

interface TaxiRepository {
    fun getTaxi(startCoordinates: Coordinates, endCoordinates: Coordinates): Single<List<Taxi>>
}