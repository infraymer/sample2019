package com.sample.android.domain.comment.repository

import io.reactivex.Single
import com.sample.android.domain.comment.model.Comment

interface CommentRepository {
    fun getComments(primepassId: String): Single<List<Comment>>
}