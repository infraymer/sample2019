package com.sample.android.domain.user.model

import android.graphics.Bitmap
import com.google.gson.annotations.SerializedName

data class User(

    @SerializedName("user_id")
    val id: Long = 0,

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("surname")
    val surname: String? = null,

    @SerializedName("phone")
    val phone: String? = null,

    @SerializedName("email")
    val email: String? = null,

    @SerializedName("avatar")
    val avatar: Bitmap? = null
)