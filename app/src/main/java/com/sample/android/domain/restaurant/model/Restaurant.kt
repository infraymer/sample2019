package com.sample.android.domain.restaurant.model

import com.sample.android.domain.comment.model.CommentItem
import com.sample.android.domain.event_list.model.EventListItem
import com.sample.android.domain.restaurant_list.model.RestaurantItem
import com.sample.android.domain.restaurant_rating.model.RestaurantRating

data class Restaurant(
    val item: RestaurantItem,
    val info: RestaurantInfo,
    val address: RestaurantAddress,
    val rating: RestaurantRating,
    val events: List<EventListItem>,
    val comments: List<CommentItem>
)