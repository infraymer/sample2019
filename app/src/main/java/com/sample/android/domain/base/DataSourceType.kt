package com.sample.android.domain.base

enum class DataSourceType {
    REMOTE, CACHE
}