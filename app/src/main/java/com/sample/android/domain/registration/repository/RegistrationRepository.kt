package com.sample.android.domain.registration.repository

import io.reactivex.Completable
import io.reactivex.Single
import com.sample.android.domain.registration.model.Registration
import com.sample.android.domain.user.model.User

interface RegistrationRepository {

    fun createUser(registration: Registration): Single<User>

    fun confirmRegistration(): Completable
}