package com.sample.android.domain.restaurant.repository

import io.reactivex.Single
import com.sample.android.domain.restaurant.model.RestaurantInfo

interface RestaurantRepository {
    fun getRestaurantInfo(restaurantId: String): Single<RestaurantInfo>
}