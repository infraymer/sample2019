package com.sample.android.domain.registration.model

import com.google.gson.annotations.SerializedName

class Registration(
    @SerializedName("phone")
    val phone: String = "",

    @SerializedName("name")
    val name: String = "",

    @SerializedName("surname")
    val surname: String = "",

    @SerializedName("type_authorization")
    val type: Type = Type.PHONE,

    @SerializedName("issue_card")
    val issueCard: Boolean = true
) {

    enum class Type {

        @SerializedName("PHONE")
        PHONE
    }
}