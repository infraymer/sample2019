package com.sample.android.domain.restaurant.model

import com.sample.android.domain.coordinates.model.Coordinates
import com.sample.android.domain.taxi.model.Taxi

data class RestaurantAddress(
    val address: String,
    val coordinates: Coordinates,
    val taxi: Taxi?
)