package com.sample.android.domain.session.repository

import io.reactivex.Single

interface SessionRepository {

    fun isFirstSession(): Single<Boolean>
}