package com.sample.android.domain.authorization.model

import com.google.gson.annotations.SerializedName

class Authorization(
    @SerializedName("login")
    val phone: String = "",

    @SerializedName("authorization_type")
    val type: Type = Type.PHONE,

    @SerializedName("password")
    val password: String? = null
) {

    enum class Type {

        @SerializedName("PHONE")
        PHONE
    }
}