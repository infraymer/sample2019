package com.sample.android.domain.comment.model

import ru.nobird.android.core.model.Identifiable
import com.sample.android.domain.user.model.User

data class CommentItem(
    val comment: Comment,
    val user: User
) : Identifiable<String> {
    override val id = comment.review
}