package com.sample.android.domain.restaurant.model

enum class RestaurantType {
    BAR, LUNCH, FAVORITE, DINER, DEFAULT
}