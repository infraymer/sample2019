package com.sample.android.domain.base.exception

open class ServerInternalError(message: String) : Throwable(message)