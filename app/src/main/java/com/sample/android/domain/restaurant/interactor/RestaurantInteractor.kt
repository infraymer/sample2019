package com.sample.android.domain.restaurant.interactor

import io.reactivex.Single
import io.reactivex.rxkotlin.Singles.zip
import io.reactivex.rxkotlin.toObservable
import com.sample.android.domain.comment.model.CommentItem
import com.sample.android.domain.comment.repository.CommentRepository
import com.sample.android.domain.event_list.repository.EventListRepository
import com.sample.android.domain.paged_list.model.NavigatorListQuery
import com.sample.android.domain.restaurant.model.Restaurant
import com.sample.android.domain.restaurant.model.RestaurantAddress
import com.sample.android.domain.restaurant.repository.RestaurantRepository
import com.sample.android.domain.restaurant_map.model.RestaurantListItem
import com.sample.android.domain.taxi.repository.TaxiRepository
import com.sample.android.domain.user.repository.UserRepository
import javax.inject.Inject

class RestaurantInteractor
@Inject
constructor(
    private val commentRepository: CommentRepository,
    private val eventListRepository: EventListRepository,
    private val restaurantInfoRepository: RestaurantRepository,
    private val taxiRepository: TaxiRepository,
    private val userRepository: UserRepository
) {
    fun getRestaurant(restaurantListItem: RestaurantListItem): Single<Restaurant> =
        zip(
            restaurantInfoRepository
                .getRestaurantInfo(restaurantListItem.id),
            getRestaurantAddress(restaurantListItem),
            eventListRepository
                .getEventListItems(NavigatorListQuery(restaurantId = restaurantListItem.id)),
            getComments(restaurantListItem.restaurantItem.primepassId)
        ) { restaurantInfo, restaurantAddress, events, comments ->
            Restaurant(
                restaurantListItem.restaurantItem,
                restaurantInfo,
                restaurantAddress,
                restaurantListItem.rating,
                events,
                comments
            )
        }

    private fun getRestaurantAddress(restaurantListItem: RestaurantListItem): Single<RestaurantAddress> =
        Single
            .just(Unit)
            .map { restaurantListItem.coordinates }
            .flatMap { taxiRepository.getTaxi(it, restaurantListItem.restaurantItem.coordinates) }
            .onErrorReturnItem(emptyList())
            .map { taxis ->
                RestaurantAddress(
                    address = restaurantListItem.restaurantItem.address,
                    coordinates = restaurantListItem.restaurantItem.coordinates,
                    taxi = taxis.firstOrNull()
                )
            }

    private fun getComments(primepassId: String): Single<List<CommentItem>> =
        commentRepository
            .getComments(primepassId)
            .flatMapObservable { it.toObservable() }
            .flatMapSingle { comment ->
                userRepository
                    .getUser(comment.userId)
                    .map { user ->
                        CommentItem(comment, user)
                    }
            }
            .toList()
            .doOnError { it.printStackTrace() }
            .onErrorReturnItem(emptyList())
}