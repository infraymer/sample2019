package com.sample.android.domain.restaurant_rating.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RestaurantRating(
    @SerializedName("rating")
    val rating: Float,

    @SerializedName("number")
    val number: Int,

    @SerializedName("place")
    val place: String
) : Parcelable