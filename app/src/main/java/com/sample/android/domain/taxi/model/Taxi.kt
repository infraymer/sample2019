package com.sample.android.domain.taxi.model

import com.google.gson.annotations.SerializedName

data class Taxi(
    @SerializedName("partner")
    val partner: String,
    @SerializedName("price")
    val price: Int,
    @SerializedName("url")
    val url: String
) {
    companion object {
        const val YANDEX = "yandex"
        const val UBER = "uber"
    }
}