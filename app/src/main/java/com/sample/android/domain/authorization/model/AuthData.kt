package com.sample.android.domain.authorization.model

import com.google.gson.annotations.SerializedName

class AuthData(

    @SerializedName("login")
    val login: String? = null,

    @SerializedName("token")
    val token: String? = null,

    @SerializedName("date")
    val date: String? = null,

    @SerializedName("user_id")
    val userId: Long = 0,

    @SerializedName("login_status")
    val loginStatus: String? = null,

    @SerializedName("login_type")
    val loginType: String? = null
)