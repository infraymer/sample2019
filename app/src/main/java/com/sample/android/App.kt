package com.sample.android

import android.app.Application
import com.sample.android.view.injection.app.AppComponent
import com.sample.android.view.injection.app.ComponentManager
import com.sample.android.view.injection.app.DaggerAppComponent
import com.sample.android.view.util.StethoHelper
import com.sample.android.view.util.extension.isMainProcess

class App : Application() {
    companion object {
        lateinit var application: App
            private set

        fun component(): AppComponent =
            application.component

        fun componentManager(): ComponentManager =
            application.componentManager
    }

    private lateinit var component: AppComponent
    private lateinit var componentManager: ComponentManager

    override fun onCreate() {
        super.onCreate()
        if (!isMainProcess) return

        StethoHelper.initStetho(this)

        application = this
        component = DaggerAppComponent.builder()
            .context(application)
            .build()
        componentManager = ComponentManager(component)
    }
}